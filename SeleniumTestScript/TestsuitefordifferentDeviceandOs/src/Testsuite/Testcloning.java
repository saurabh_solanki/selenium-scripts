package Testsuite;
import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.Assert;
import org.testng.AssertJUnit;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

@SuppressWarnings("unused")
public class Testcloning {
	static PropertiesConfiguration configuration;
	static {
		final String PROPERTIES_FILE_NAME = "C:\\Users\\Saurabh\\workspace\\VideoUs\\conf\\configuration.properties";
		configuration = new PropertiesConfiguration();
		try {
			configuration.load(PROPERTIES_FILE_NAME);
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}

	}
	/**
	 * @return the configuration
	 */
	public static PropertiesConfiguration getConfiguration() {
		return configuration;
	}

	/**
	 * @param configuration the configuration to set
	 */
	public static void setConfiguration(PropertiesConfiguration configuration) {
		Testcloning.configuration = configuration;
	}

	/**
	 * @return the driver
	 */
	public WebDriver getDriver() {
		return driver;
	}

	/**
	 * @param driver the driver to set
	 */
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	/**
	 * @return the baseUrl
	 */
	public String getBaseUrl() {
		return baseUrl;
	}

	/**
	 * @param baseUrl the baseUrl to set
	 */
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	/**
	 * @return the verificationErrors
	 */
	public StringBuffer getVerificationErrors() {
		return verificationErrors;
	}

	/**
	 * @param verificationErrors the verificationErrors to set
	 */
	public void setVerificationErrors(StringBuffer verificationErrors) {
		this.verificationErrors = verificationErrors;
	}

	/**
	 * @return the result
	 */
	public static String getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public static void setResult(String result) {
		Testcloning.result = result;
	}

	/**
	 * @return the campaign
	 */
	public String getCampaign() {
		return campaign;
	}

	/**
	 * @param campaign the campaign to set
	 */
	public void setCampaign(String campaign) {
		this.campaign = campaign;
	}

	/**
	 * @return the search
	 */
	public String getSearch() {
		return search;
	}

	/**
	 * @param search the search to set
	 */
	public void setSearch(String search) {
		this.search = search;
	}

	/**
	 * @return the finalDate
	 */
	public String getFinalDate() {
		return finalDate;
	}

	/**
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}
  private WebDriver driver;
  private String baseUrl;
  private StringBuffer verificationErrors = new StringBuffer();
  private static String result;
  private String campaign;
  public String search;
  public String finalDate;

@BeforeTest
  public void setUp() throws Exception {

	System.setProperty("webdriver.firefox.bin",
			(String) configuration.getProperty("webdriver.firefox.bin"));
    driver = new FirefoxDriver();
    baseUrl = "https://mobile.sb.vdopia.com";
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test(priority = 0)
  public void GmailLogin() throws Exception {
	    
	  //  DateFormat dateFormat2 = new SimpleDateFormat("dd"); 
	  //  Date date2 = new Date();
	    
	    driver.get(baseUrl + "/vadmins/?page=login");
		driver.findElement(By.id("googleLogin")).click();
	    driver.findElement(By.id("Email")).clear();
	    String user = (String) configuration.getProperty("gmail.name");
		String Pass = (String) configuration.getProperty("gmail.password");
	    driver.findElement(By.id("Email")).sendKeys(user);
	    driver.findElement(By.id("Passwd")).clear();
	    driver.findElement(By.id("Passwd")).sendKeys(Pass);
	    driver.findElement(By.id("signIn")).click();
  }  
  @Test(priority = 1)
	public void PublisherSearch() {
	  
	    driver.findElement(By.linkText("Publishers")).click();
	    driver.findElement(By.id("searchtext")).sendKeys("saurabh");
	    driver.findElement(By.name("action_search")).click();
	    driver.findElement(By.linkText("saurabh.solanki@vdopia.com")).click();
	    driver.close();    
       }
  
  @Test(priority = 2)
	public void ChannelCreation() {
	  
           DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd:HH:mm"); 
           Date date = new Date();
           finalDate = dateFormat.format(date);
  
	    for(String winHandle : driver.getWindowHandles()) {
	        driver.switchTo().window(winHandle);
	      }
	    driver.findElement(By.cssSelector("b")).click();
	    //System.out.print(FinalDate);
	    driver.findElement(By.id("name")).clear();
	    driver.findElement(By.id("name")).sendKeys("App on" + (finalDate));
	    // ERROR: Caught exception [Error: Dom locators are not implemented yet!]
	    driver.findElement(By.id("app_type_web")).click();
	    driver.findElement(By.id("url")).clear();
	    driver.findElement(By.id("url")).sendKeys("www.testingworld.com");
	    driver.findElement(By.id("submit")).click();
	    driver.findElement(By.xpath("(//a[contains(text(),'Test Mode')])[last()]")).click();
	    driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[last()]/td[2]/div/input")).click();
	    search = driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[4]")).getText();
  }
  
  @Test(priority = 3)
	public void AdvertiserLogin() throws IOException {
	  String url2 = (String) configuration.getProperty("url.name2");
	  getDriver().get(getBaseUrl() + url2);
      driver.findElement(By.linkText("Advertisers")).click();
       //driver.findElement(By.id("filtertext")).clear();
       driver.findElement(By.id("searchtext")).sendKeys("saurabh");
	   driver.findElement(By.name("action_search")).click();
	   driver.findElement(By.linkText("saurabh.solanki@vdopia.com")).click();
	   driver.close();
    for(String winHandle : driver.getWindowHandles()){
        driver.switchTo().window(winHandle);
      }
  }
    //System.out.println("Title of the page after - switchingTo: " + driver.getTitle());
  
    @Test(priority = 4)
	public void CreateAnimatedBanner() {
    driver.findElement(By.xpath("//input[@value='Create New Ad Campaign']")).click();
    driver.findElement(By.id("htmlbanneradvertisement")).click();
    driver.findElement(By.cssSelector("input.submit")).click();
    driver.findElement(By.id("weburl")).click();
    driver.findElement(By.id("jsadurl")).clear();
    driver.findElement(By.id("jsadurl")).sendKeys("http://i2.vdopia.com/dev/pankaj_adserver/advertisements/feb13/TUPSS_NCAA/AnimatedBanner/animatedBanner");
    driver.findElement(By.id("bannername")).clear();
    driver.findElement(By.id("bannername")).sendKeys("Animated on 15");
    new Select(driver.findElement(By.id("imgsize"))).selectByVisibleText("320x48");
    driver.findElement(By.id("desturl")).clear();
    driver.findElement(By.id("desturl")).sendKeys("http://m.yahoo.com");
    driver.findElement(By.id("saveButtom")).click();
    driver.findElement(By.xpath("//input[@value='Next']")).click(); 
  }
    @Test(priority = 5)
	public void Targeting() throws InterruptedException {
    driver.findElement(By.id("choosegeotargeting")).click();
    driver.findElement(By.id("cntIN")).click();
    driver.findElement(By.id("choosesitetargeting")).click();
    driver.findElement(By.id("sss")).sendKeys((search));
    Thread.sleep(20000);
    //Select selectBox = new Select(driver.findElement(By.id("site1")));
    driver.findElement(By.id("site1")).sendKeys((search));
    // Targeting on App
   // selectBox.selectByIndex([last()]);
    driver.findElement(By.xpath("//input[@value=' Add>>     ']")).click();
    Thread.sleep(20000);
    //Thread.sleep(20000);
    
    driver.findElement(By.id("submit")).click();
    }
    @Test(priority = 6)
	public void Budget() {
    new Select(driver.findElement(By.id("model"))).selectByVisibleText("CPC");
    driver.findElement(By.id("maxcpm")).clear();
    driver.findElement(By.id("maxcpm")).sendKeys("11");
    driver.findElement(By.id("dailylimit")).clear();
    driver.findElement(By.id("dailylimit")).sendKeys("11");
    driver.findElement(By.id("budget")).clear();
    driver.findElement(By.id("budget")).sendKeys("111");
    new Select(driver.findElement(By.name("priority_level"))).selectByVisibleText("Backfill");
    new Select(driver.findElement(By.name("pacing_type"))).selectByVisibleText("Uniform");
    new Select(driver.findElement(By.name("timezone"))).selectByVisibleText("IST (Indian Standard Time)");
    driver.findElement(By.id("start_date")).click();
    DateFormat dateFormat2 = new SimpleDateFormat("dd"); 
    Date date2 = new Date();
	String today = dateFormat2.format(date2);
	System.out.print(today);
    //driver.findElement(By.xpath("//div[@id='dp-calendar']/table/tbody/tr[3]/td[5]")).click();
    WebElement dateWidget = driver.findElement(By.id("dp-calendar"));  
    List<WebElement> columns=dateWidget.findElements(By.tagName("td"));  

	for (WebElement cell : columns) {
	if (cell.getText().equals(today)) {
	cell.click();
	break;
	}

	}
    //driver.findElement(By.xpath("//div[@id='dp-calendar']/table/tbody/tr[4]/td[3]")).click();
    driver.findElement(By.id("end_date")).click();
    driver.findElement(By.xpath("//div[@id='dp-calendar']/table/tbody/tr[5]/td[4]")).click();
    driver.findElement(By.id("frequencyperday")).clear();
    driver.findElement(By.id("frequencyperday")).sendKeys("1");
    driver.findElement(By.id("frequencyperweek")).clear();
    driver.findElement(By.id("frequencyperweek")).sendKeys("7");
    driver.findElement(By.id("frequencypermonth")).clear();
    driver.findElement(By.id("frequencypermonth")).sendKeys("35");
    driver.findElement(By.id("action_submit")).click();
    }
    
    @Test(priority = 7)
	public void CampaignCreation() {
    driver.findElement(By.id("campaignname")).clear();
    driver.findElement(By.id("campaignname")).sendKeys("Animated Banner on "+(finalDate));
    String campaign = driver.findElement(By.id("campaignname")).getAttribute("value");
    System.out.println(campaign);
    driver.findElement(By.id("submit")).click();
    Alert javascriptAlert = driver.switchTo().alert();
    System.out.println(javascriptAlert.getText()); // Get text on alert box
    javascriptAlert.accept();
    driver.findElement(By.name("action_approve")).click();
    }
    //Thread.sleep(240000);
 // Connecting to Databse in order to Select Campaign Id
 	/**
 	 * 
 	 * @throws InterruptedException
 	 * @throws IOException
 	 */
 	@Test(priority = 8)
	public void Memcamp() throws InterruptedException, IOException {

	driver.get("https://sb.vdopia.com/vadmins/index.php?page=login");
    driver.findElement(By.id("googleLogin")).click();
    //driver.findElement(By.id("googleLogin")).click();
    String query = "select * from memcamp where cid in (select id from campaign where name = ";
    String second = query + "'" + campaign + "');";
    System.out.print(second);
    driver.findElement(By.linkText("Tools")).click();
    
    // Please Remove Below Comments When Used in Live Portal
    //driver.findElement(By.id("googleLogin")).click();
   // driver.findElement(By.linkText("Tools")).click();
    driver.findElement(By.linkText("Query Browser Live")).click();
    driver.findElement(By.name("query")).clear();
    driver.findElement(By.name("query")).sendKeys(second);
    Thread.sleep(240000);
    driver.findElement(By.name("runsubmit")).click();
    String result = driver.findElement(By.xpath("/html/body/div/div[3]/div/div/div/div/div/div[2]/div[2]/div/table/tbody/tr/td")).getText();
    System.out.print(result);
    try {
        assertEquals("996", driver.findElement(By.cssSelector("#DivMainContent > #TblId > #fi-approvaltable > tbody > tr > td")).getText());
    } catch (Error e) {
        verificationErrors.append(e.toString());
      }
    Thread.sleep(5000);
}
 	
 	@Test(priority = 10)
	public void Clone() throws InterruptedException
 	
 	{
 		
 	driver.get("https://mobile.sb.vdopia.com/adv.php?page=campaign&sub=campaignsummary");
 	driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr/td/table/tbody/tr/td/form[3]/table[2]/tbody/tr/td[2]/a[2]/img")).click();
 	Thread.sleep(5000);
 	//driver.findElement(By.cssSelector("img[title=\"Create Clone\"]")).click();
 	driver.findElement(By.id("campaignname")).clear();
    driver.findElement(By.id("campaignname")).sendKeys("Cloned Animated Banner on "+(finalDate));
    new Select(driver.findElement(By.id("campaignmodel"))).selectByVisibleText("CPM");
    driver.findElement(By.name("action_saveclone")).click();
	//driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr/td/table/tbody/tr/td/form[3]/table[2]/tbody/tr/td[2]/a[2]/img")).click();
    driver.findElement(By.cssSelector("img[title=\"Edit Ads\"]")).click();
    driver.findElement(By.id("campaignname")).clear();
    driver.findElement(By.id("campaignname")).sendKeys("Cloned Animated Banner on "+(finalDate));
    String campaign1 = driver.findElement(By.id("campaignname")).getAttribute("value");
    System.out.println(campaign);
    driver.findElement(By.id("submit")).click();
    Alert javascriptAlert = driver.switchTo().alert();
    System.out.println(javascriptAlert.getText()); // Get text on alert box
    javascriptAlert.accept();
    driver.findElement(By.name("action_approve")).click();
 	}
 	
 	@Test(priority = 11)
 	public void Memcamp1() throws InterruptedException, IOException {

 		// String driver = props.getProperty("jdbc.driver");
 		String url =(String) configuration.getProperty("jdbc.url");
 		String username = (String) configuration.getProperty("jdbc.username");
 		String password = (String) configuration.getProperty("jdbc.password");
 		String dbClass = (String) configuration.getProperty("jdb.class");

 		/*
 		 * String dbUrl = "jdbc:mysql://sb.vdopia.com:3306/adplatform"; String
 		 * username="saurabh"; String password="saurabh4114VDO"; String dbClass
 		 * = "com.mysql.jdbc.Driver"; String query =
 		 * "select id from campaign where name = ''+ 'campaign'";
 		 */
 		String query = "select id from campaign where name = ''+ 'campaign1'";

 		try {

 			Class.forName(dbClass);
 			// Connection con = DriverManager.getConnection
 			// (dbUrl,username,password);
 			Connection con = DriverManager.getConnection(url, username,
 					password);
 			Statement stmt = con.createStatement();
 			ResultSet rs = stmt.executeQuery(query);
 			ResultSetMetaData rsmd = rs.getMetaData();
 			int columnsNumber = rsmd.getColumnCount();

 			while (rs.next()) {
 				// Print one row
 				for (int i2 = 1; i2 <= columnsNumber; i2++) {
 					result = rs.getString(i2) + " ";
 					// System.out.println(result);
 					// System.out.println();//Move to the next line to print the
 					// next row.

 				} // end while
 			}
 			con.close();
 		} catch (ClassNotFoundException e) {
 			e.printStackTrace();
 		} catch (SQLException e) {
 			e.printStackTrace();
 		}

 	// Checking Entry In Memcamp
 			String url3 = (String) configuration.getProperty("url.name3");
 			getDriver().get(getBaseUrl() + url3);
 			//driver.get(baseUrl + url3);
 			Thread.sleep(Long.valueOf((String) configuration
 					.getProperty("thread.wait.time3")));
 			driver.findElement(By.id("cid")).sendKeys((result));
 			driver.findElement(By.name("submit")).click();
 			try {
 				AssertJUnit.assertTrue(driver.findElement(By.cssSelector("BODY"))
 						.getText()
 						.matches("^[\\s\\S]*\\[\"advertiser_id\"\\][\\s\\S]*$"));
 			} catch (Error e) {
 				verificationErrors.append(e.toString());
 				throw e;
 			}
 			Thread.sleep(Long.valueOf((String) configuration
 					.getProperty("thread.wait.time1")));
 		}
 	
 	
 	@Test(priority = 11)
	public void TagServing() throws InterruptedException, IOException {
		String url4 = (String) configuration.getProperty("url.name4");
		getDriver().get(getBaseUrl() + url4);
   //driver.get("https://mobile.sb.vdopia.com/vadmins/?page=prescreen");
   //driver.get("https://mobile.sb.vdopia.com/pub.php");
   driver.findElement(By.linkText("Publishers")).click();
   driver.findElement(By.id("searchtext")).sendKeys("saurabh");
   driver.findElement(By.name("action_search")).click();
   driver.findElement(By.linkText("saurabh.solanki@vdopia.com")).click();
   driver.close();
  for(String winHandle : driver.getWindowHandles()){
      driver.switchTo().window(winHandle);
   }
   driver.findElement(By.cssSelector("b")).click();
   driver.findElement(By.linkText("View App")).click();
   
   //System.out.print(select);
 // abc = driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[9]/select")).getText();
  new Select(driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[9]/select"))).selectByVisibleText("Transparent Rich Media Takeover");
  driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[9]/input[4]")).click();
  Thread.sleep(5000);
  String var = driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[9]/span/textarea")).getText();
  System.out.println(var);
  String var1 = var.substring(var.indexOf("<script language='javascript'")+35, var.lastIndexOf("</script>")-2);
  System.out.println(var1);
  //var2 = var1.substring(var.indexOf("src='"), var1.lastIndexOf("'>"));
 // System.out.println(var2);
  //new Select(driver.findElement(By.id("tagValue34184"))).selectByVisibleText("https://sb.vdopia.com/adserver/html5/inwapads/?sleepAfter=0;adFormat=banner;ak=77435ddf6ca1eff7503275016983f42e;version=1.0;cb=[timestamp]");
 // id="tagValue34184"
  //driver.findElement(By.name("type")).click();
  String url5 = (String) configuration.getProperty("url.name5");
	driver.get(url5);
  driver.findElement(By.id("tag")).clear();
  driver.findElement(By.id("tag")).sendKeys(var1);
  driver.findElement(By.name("type")).click();
  driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
  try {
      AssertJUnit.assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*inventory available[\\s\\S]*$"));
   } catch (Error e) {
     verificationErrors.append(e.toString());
     throw e;
  }
  Thread.sleep(Long.valueOf((String) configuration
			.getProperty("thread.wait.time1")));
 	}
 	
 	@Test(priority = 12)
	public void Logout() {
		driver.get(baseUrl + "/vadmins/?page=login");
		driver.findElement(By.linkText("Logout")).click();
	}

@AfterTest
public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      Assert.fail(verificationErrorString);
    }
  }
}
