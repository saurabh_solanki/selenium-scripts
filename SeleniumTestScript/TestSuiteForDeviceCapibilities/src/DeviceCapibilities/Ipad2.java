package DeviceCapibilities;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.Select;

public class Ipad2 {
  private WebDriver driver;
  private String baseUrl;
  private StringBuffer verificationErrors = new StringBuffer();
  private String Publisher;
  
@Before
  public void setUp() throws Exception {
	FirefoxProfile firefoxProfile = new ProfilesIni().getProfile("default");
	firefoxProfile.setPreference("general.useragent.override","Mozilla/5.0(iPad; U; CPU OS 4_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8F191 Safari/6533.18.5");
	driver = new FirefoxDriver(firefoxProfile);
	baseUrl = "https://mobile.sb.vdopia.com";
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testAnimatedBanner() throws Exception {
	  DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy:HH:mm"); 
	    Date date = new Date();
	    String FinalDate = dateFormat.format(date);
	    System.out.print(FinalDate);
	  //  DateFormat dateFormat2 = new SimpleDateFormat("dd"); 
	  //  Date date2 = new Date();
	    
	    driver.get(baseUrl + "/vadmins/?page=login");
		driver.findElement(By.id("googleLogin")).click();
	    driver.findElement(By.id("Email")).clear();
	    driver.findElement(By.id("Email")).sendKeys("ads@vdopia.com");
	    driver.findElement(By.id("Passwd")).clear();
	    driver.findElement(By.id("Passwd")).sendKeys("adsVDO123");
	    driver.findElement(By.id("signIn")).click();
	    driver.findElement(By.linkText("Publishers")).click();
	    driver.findElement(By.id("searchtext")).sendKeys("saurabh");
	    driver.findElement(By.name("action_search")).click();
	    driver.findElement(By.linkText("saurabh.solanki@vdopia.com")).click();
	    Publisher = driver.findElement(By.linkText("saurabh.solanki@vdopia.com")).getText();
	    System.out.print(Publisher);
	    driver.close();
	    for(String winHandle : driver.getWindowHandles()){
	        driver.switchTo().window(winHandle);
	      }
	    driver.findElement(By.cssSelector("b")).click();
	    //System.out.print(FinalDate);
	    driver.findElement(By.id("name")).clear();
	    driver.findElement(By.id("name")).sendKeys("App on" + (FinalDate));
	    // ERROR: Caught exception [Error: Dom locators are not implemented yet!]
	    driver.findElement(By.id("app_type_web")).click();
	    driver.findElement(By.name("tablet_specific")).click();
	    driver.findElement(By.id("url")).clear();
	    driver.findElement(By.id("url")).sendKeys("www.testingworld.com");
	    driver.findElement(By.id("submit")).click();
	    driver.findElement(By.xpath("(//a[contains(text(),'Test Mode')])[last()]")).click();
	    driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[last()]/td[2]/div/input")).click();
	   String search = driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[4]")).getText();
	   System.out.print(search);
	   driver.get("https://mobile.sb.vdopia.com/vadmins/?page=prescreen");
       driver.findElement(By.linkText("Advertisers")).click();
       //driver.findElement(By.id("filtertext")).clear();
       driver.findElement(By.id("searchtext")).sendKeys("saurabh");
	    driver.findElement(By.name("action_search")).click();
	    driver.findElement(By.linkText("saurabh.solanki@vdopia.com")).click();
	    driver.close();
    //driver.findElement(By.xpath("//input[@value='Create New Ad Campaign']")).click();
 
       //Opening Dashboard of Advertiser Account
    for(String winHandle : driver.getWindowHandles()){
        driver.switchTo().window(winHandle);
      }
    //System.out.println("Title of the page after - switchingTo: " + driver.getTitle());
    
    //driver.findElement(By.linkText("Create campaign")).click();
    driver.findElement(By.xpath("//input[@value='Create New Ad Campaign']")).click();
    driver.findElement(By.id("htmlbanneradvertisement")).click();
    driver.findElement(By.cssSelector("input.submit")).click();
    
    driver.findElement(By.id("weburl")).click();
    driver.findElement(By.id("jsadurl")).clear();
    driver.findElement(By.id("jsadurl")).sendKeys("http://i2.vdopia.com/dev/pankaj_adserver/advertisements/feb13/TUPSS_NCAA/AnimatedBanner/animatedBanner");
    driver.findElement(By.id("bannername")).clear();
    driver.findElement(By.id("bannername")).sendKeys("Animated on 15");
    //driver.findElement(By.id("htmlpreview")).click();
    //assertEquals("Please select size also before click on preview button.", closeAlertAndGetItsText());
    new Select(driver.findElement(By.id("imgsize"))).selectByVisibleText("320x48");
   // driver.findElement(By.id("htmlpreview")).click();
    //
    //Alert javascriptAlert = driver.switchTo().alert();
    //System.out.println(javascriptAlert.getText()); // Get text on alert box
    //javascriptAlert.accept();
    driver.findElement(By.id("desturl")).clear();
    driver.findElement(By.id("desturl")).sendKeys("http://m.yahoo.com");
    driver.findElement(By.id("saveButtom")).click();
    driver.findElement(By.xpath("//input[@value='Next']")).click();
    driver.findElement(By.id("choosegeotargeting")).click();
    driver.findElement(By.id("cntIN")).click();
    driver.findElement(By.id("choosesitetargeting")).click();
    driver.findElement(By.id("sss")).sendKeys((search));
    Thread.sleep(20000);
    driver.findElement(By.id("sss")).sendKeys(Keys.RETURN);
    driver.findElement(By.id("site1")).sendKeys((search));
    String Channel1 = "span[title=\"" + search + " (Vdopia->" + Publisher + ")\"]";
    System.out.print(Channel1);
    driver.findElement(By.cssSelector(Channel1)).click();
    //Thread.sleep(20000);
    driver.findElement(By.cssSelector("input.leftarrow")).click();
    Thread.sleep(20000);
    //Thread.sleep(20000);
    driver.findElement(By.id("imgat10")).click();
    driver.findElement(By.xpath("//input[@value='tablets']")).click();
    driver.findElement(By.id("submit")).click();
    new Select(driver.findElement(By.id("model"))).selectByVisibleText("CPC");
    driver.findElement(By.id("maxcpm")).clear();
    driver.findElement(By.id("maxcpm")).sendKeys("11");
    driver.findElement(By.id("dailylimit")).clear();
    driver.findElement(By.id("dailylimit")).sendKeys("11");
    driver.findElement(By.id("budget")).clear();
    driver.findElement(By.id("budget")).sendKeys("111");
    new Select(driver.findElement(By.name("priority_level"))).selectByVisibleText("Backfill");
    new Select(driver.findElement(By.name("pacing_type"))).selectByVisibleText("Uniform");
    new Select(driver.findElement(By.name("timezone"))).selectByVisibleText("IST (Indian Standard Time)");
    driver.findElement(By.id("start_date")).click();
    DateFormat dateFormat2 = new SimpleDateFormat("dd"); 
    Date date2 = new Date();
	String today = dateFormat2.format(date2);
	System.out.print(today);
    //driver.findElement(By.xpath("//div[@id='dp-calendar']/table/tbody/tr[3]/td[5]")).click();
    WebElement dateWidget = driver.findElement(By.id("dp-calendar"));  
    List<WebElement> columns=dateWidget.findElements(By.tagName("td"));  

	for (WebElement cell : columns) {
	if (cell.getText().equals(today)) {
	cell.click();
	break;
	}

	}

    //driver.findElement(By.xpath("//div[@id='dp-calendar']/table/tbody/tr[4]/td[3]")).click();
    driver.findElement(By.id("end_date")).click();
    driver.findElement(By.xpath("//div[@id='dp-calendar']/table/tbody/tr[5]/td[3]")).click();
    driver.findElement(By.id("frequencyperday")).clear();
    driver.findElement(By.id("frequencyperday")).sendKeys("1");
    driver.findElement(By.id("frequencyperweek")).clear();
    driver.findElement(By.id("frequencyperweek")).sendKeys("7");
    driver.findElement(By.id("frequencypermonth")).clear();
    driver.findElement(By.id("frequencypermonth")).sendKeys("35");
    driver.findElement(By.id("action_submit")).click();
    driver.findElement(By.id("campaignname")).clear();
    driver.findElement(By.id("campaignname")).sendKeys("Animated Banner on "+(FinalDate));
    String campaign = driver.findElement(By.id("campaignname")).getAttribute("value");
    System.out.println(campaign);
    driver.findElement(By.id("submit")).click();
    Alert javascriptAlert = driver.switchTo().alert();
    System.out.println(javascriptAlert.getText()); // Get text on alert box
    javascriptAlert.accept();
    driver.findElement(By.name("action_approve")).click();
    driver.get("https://sb.vdopia.com/vadmins/index.php?page=login");
    driver.findElement(By.id("googleLogin")).click();
    //driver.findElement(By.id("googleLogin")).click();
    String query = "select * from memcamp where cid in (select id from campaign where name = ";
    String second = query + "'" + campaign + "');";
    System.out.print(second);
    driver.findElement(By.linkText("Tools")).click();
    
    // Please Remove Below Comments When Used in Live Portal
    //driver.findElement(By.id("googleLogin")).click();
   // driver.findElement(By.linkText("Tools")).click();
    driver.findElement(By.linkText("Query Browser Live")).click();
    driver.findElement(By.name("query")).clear();
    driver.findElement(By.name("query")).sendKeys(second);
    Thread.sleep(240000);
    driver.findElement(By.name("runsubmit")).click();
    String result = driver.findElement(By.xpath("/html/body/div/div[3]/div/div/div/div/div/div[2]/div[2]/div/table/tbody/tr/td")).getText();
    System.out.print(result);
    try {
        assertEquals("996", driver.findElement(By.cssSelector("#DivMainContent > #TblId > #fi-approvaltable > tbody > tr > td")).getText());
    } catch (Error e) {
        verificationErrors.append(e.toString());
      }
    Thread.sleep(5000);
   driver.get("https://mobile.sb.vdopia.com/vadmins/?page=prescreen");
   //driver.get("https://mobile.sb.vdopia.com/pub.php");
   driver.findElement(By.linkText("Publishers")).click();
   driver.findElement(By.id("searchtext")).sendKeys("saurabh");
   driver.findElement(By.name("action_search")).click();
   driver.findElement(By.linkText("saurabh.solanki@vdopia.com")).click();
   driver.close();
  for(String winHandle : driver.getWindowHandles()){
      driver.switchTo().window(winHandle);
   }
   driver.findElement(By.cssSelector("b")).click();
   driver.findElement(By.linkText("View App")).click();
   
   //System.out.print(select);
 // abc = driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[9]/select")).getText();
  new Select(driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[9]/select"))).selectByVisibleText("Transparent Rich Media Takeover");
  driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[9]/input[4]")).click();
  Thread.sleep(50000);
  String var = driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[9]/span/textarea")).getText();
  System.out.println(var);
  String var1 = var.substring(var.indexOf("<script language='javascript'")+35, var.lastIndexOf("</script>")-2);
  System.out.println(var1);
  driver.get(var1);
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"inventory\":\"available\"[\\s\\S]*$"));
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}
	String contain = driver.findElement(By.cssSelector("BODY")).getText();
	String split = contain.substring(
				contain.indexOf("iVDOAds({") + 35,
				contain.lastIndexOf("'iVDOAds');") - 35);
	  String Tag1 = "vdo_tvi";
	  String Tag2 = "vdo_tvi_mu";
	  String Tag3 = "vdo_tcl";
		System.out.print(split);
		if(split.contains(Tag1))
		{
		try {
			assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
					.matches("^[\\s\\S]*\"vdo_tvi\"[\\s\\S]*$"));
			//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
		} catch (Error e) {
			verificationErrors.append(e.toString());
			throw e;
		}	
		}
		else
		{
			System.out.print("Tag vdo_tvi is not present");
		}
	if(split.contains(Tag2))
	{
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"vdo_tvi_mu\"[\\s\\S]*$"));
		//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}	
	}
	else
	{
		System.out.print("Tag vdo_tvi_mu not found");
	}
	if(split.contains(Tag3))
	{
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"vdo_tcl\"[\\s\\S]*$"));
		//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}	
	}
	else
	{
		System.out.print("Tag vdo_tcl not found");
	}
		//Thread.sleep(Long.valueOf((String) configuration
			//	.getProperty("thread.wait.time1")));
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
	
	 	
	Thread.sleep(5000);
    driver.get(baseUrl + "/vadmins/?page=login");
    driver.findElement(By.linkText("Logout")).click();

  }


@After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      Assert.fail(verificationErrorString);
    }
  }
}
