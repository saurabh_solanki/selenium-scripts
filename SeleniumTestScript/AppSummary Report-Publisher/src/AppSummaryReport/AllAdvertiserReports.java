package AppSummaryReport;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.*;

import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
  public class AllAdvertiserReports {
  private WebDriver driver;
  private String baseUrl;
  private StringBuffer verificationErrors = new StringBuffer();
  private WebElement row;
  private WebElement table2;
  private WebElement element;
  private List<WebElement> secondColumns;
  private String impressions;
  private String revenue;
  private WebElement table;
private WebElement table1;
private WebElement appSummarytable;
private WebElement countrySummarytable;
private WebElement adFormattable;

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://mobile.vdopia.com";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testAllAdvertiserReports() throws Exception {
	  driver.get(baseUrl + "/vadmins/?page=login");
	    driver.findElement(By.id("googleLogin")).click();
	    driver.findElement(By.id("Email")).clear();
	    driver.findElement(By.id("Email")).sendKeys("saurabh.solanki@vdopia.com");
	    driver.findElement(By.id("Passwd")).clear();
	    driver.findElement(By.id("Passwd")).sendKeys("sau123456");
	    driver.findElement(By.id("signIn")).click();
	    driver.findElement(By.linkText("Advertisers")).click();
	    driver.findElement(By.id("searchtext")).clear();
	    driver.findElement(By.id("searchtext")).sendKeys("srikantk");
	    driver.findElement(By.name("action_search")).click();
	    driver.findElement(By.linkText("srikantk@vdopia.com")).click();
	    for(String winHandle : driver.getWindowHandles()) 
	    {
	        driver.switchTo().window(winHandle);
	    }
// Campaign Performance 
  driver.findElement(By.linkText("Reports")).click();
  new Select(driver.findElement(By.id("reporttype"))).selectByVisibleText("Campaign Performance Report");
  driver.findElement(By.id("daterange")).clear();
  driver.findElement(By.id("daterange")).sendKeys("01-Sep-2013 to 30-Sep-2013");
  driver.findElement(By.id("daterange")).sendKeys(Keys.ENTER);
  driver.findElement(By.id("daterange")).sendKeys(Keys.ENTER);
  driver.findElement(By.id("run")).click();
  table = driver.findElement(By.id("advreoprt"));
  String impressions = driver.findElement(By.xpath("//table[@id='advreoprt']/tfoot/tr[last()]/td[2]/b")).getText();
  System.out.print(impressions);
  String clicks = driver.findElement(By.xpath("//table[@id='advreoprt']/tfoot/tr[last()]/td[3]/b")).getText();
  System.out.print(clicks);
  String revenue = driver.findElement(By.xpath("//table[@id='advreoprt']/tfoot/tr[last()]/td[5]/b")).getText();
  System.out.print(revenue);
    
      //Creative Performance Report
  //driver.findElement(By.linkText("Reports")).click();
  new Select(driver.findElement(By.id("reporttype"))).selectByVisibleText("Creative Performance Report");
  driver.findElement(By.id("daterange")).clear();
  driver.findElement(By.id("daterange")).sendKeys("01-Sep-2013 to 30-Sep-2013");
  driver.findElement(By.id("daterange")).sendKeys(Keys.ENTER);
  driver.findElement(By.id("daterange")).sendKeys(Keys.ENTER);
  driver.findElement(By.id("run")).click();
  appSummarytable = driver.findElement(By.id("advreoprt"));
  String CraetivePerfromanceimpressions = driver.findElement(By.xpath("//table[@id='advreoprt']/tbody/tr[last()]/td[3]")).getText();
  System.out.print(CraetivePerfromanceimpressions);
  String AppSummaryclicks = driver.findElement(By.xpath("//table[@id='advreoprt']/tbody/tr[last()]/td[4]")).getText();
  System.out.print(AppSummaryclicks);
  String AppSummaryrevenue = driver.findElement(By.xpath("//table[@id='advreoprt']/tbody/tr[last()]/td[6]")).getText();
  System.out.print(AppSummaryrevenue);
  if (impressions.equals(CraetivePerfromanceimpressions))
  {
      System.out.print("Impressions are matched");
  }
  else
  {
	  System.out.print("Impressions are not matched");
  }
  
  if (clicks.equals(AppSummaryclicks))
  {
      System.out.print("Clicks are matched");
  }
  else
  {
	  System.out.print("Clicks are not matched");
  }
  if (revenue.equals(AppSummaryrevenue))
  {
      System.out.print("Revenue are matched");
  }
  else
  {
	  System.out.print("Revenue are not matched");
  }
  
  // Country Summary Report
  //driver.findElement(By.linkText("Reports")).click();
  new Select(driver.findElement(By.id("reporttype"))).selectByVisibleText("Country Performance Report");
  //driver.findElement(By.id("daterange")).clear();
  driver.findElement(By.id("daterange")).sendKeys("01-Sep-2013 to 30-Sep-2013");
  driver.findElement(By.id("daterange")).sendKeys(Keys.ENTER);
  driver.findElement(By.id("daterange")).sendKeys(Keys.ENTER);
  driver.findElement(By.id("run")).click();
  countrySummarytable = driver.findElement(By.id("advreoprt"));
  String CountrySummaryimpressions = driver.findElement(By.xpath("//table[@id='advreoprt']/tbody/tr[last()]/td[2]")).getText();
  System.out.print(CountrySummaryimpressions);
  String CountrySummaryclicks = driver.findElement(By.xpath("//table[@id='advreoprt']/tbody/tr[last()]/td[3]")).getText();
  System.out.print(CountrySummaryclicks);
  String CountrySummaryrevenue = driver.findElement(By.xpath("//table[@id='advreoprt']/tbody/tr[last()]/td[5]")).getText();
  System.out.print(CountrySummaryrevenue);
  if (impressions.equals(CountrySummaryimpressions))
  {
      System.out.print("Impressions are matched");
  }
  else
  {
	  System.out.print("Impressions are not matched");
  }
  
  if (clicks.equals(CountrySummaryclicks))
  {
      System.out.print("Clicks are matched");
  }
  else
  {
	  System.out.print("Clicks are not matched");
  }
  if (revenue.equals(CountrySummaryrevenue))
  {
      System.out.print("Revenue are matched");
  }
  else
  {
	  System.out.print("Revenue are not matched");
  }
  
  //State Perfromance Report
  
  driver.findElement(By.linkText("Reports")).click();
  new Select(driver.findElement(By.id("reporttype"))).selectByVisibleText("State Performance Report");
  driver.findElement(By.id("daterange")).clear();
  driver.findElement(By.id("daterange")).sendKeys("01-Sep-2013 to 30-Sep-2013");
  driver.findElement(By.id("daterange")).sendKeys(Keys.ENTER);
 // driver.findElement(By.id("daterange")).sendKeys(Keys.ENTER);
  driver.findElement(By.id("run")).click();
  adFormattable = driver.findElement(By.id("advreoprt"));
  String AdFormatimpressions = driver.findElement(By.xpath("//table[@id='advreoprt']/tbody/tr[last()]/td[2]")).getText();
  System.out.print(AdFormatimpressions);
  String AdFormatclicks = driver.findElement(By.xpath("//table[@id='advreoprt']/tbody/tr[last()]/td[3]")).getText();
  System.out.print(AdFormatclicks);
  String AdFormatrevenue = driver.findElement(By.xpath("//table[@id='advreoprt']/tbody/tr[last()]/td[5]")).getText();
  System.out.print(AdFormatrevenue);
  
  if (impressions.equals(AdFormatimpressions))
  {
      System.out.print("Impressions are matched");
  }
  else
  {
	  System.out.print("Impressions are not matched");
  }
  
  if (clicks.equals(AdFormatclicks))
  {
      System.out.print("Clicks are matched");
  }
  else
  {
	  System.out.print("Clicks are not matched");
  }
  if (revenue.equals(AdFormatrevenue))
  {
      System.out.print("Revenue are matched");
  }
  else
  {
	  System.out.print("Revenue are not matched");
  }
  }
  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

public WebElement getTable2() {
	return table2;
}

public void setTable2(WebElement table2) {
	this.table2 = table2;
}
}
