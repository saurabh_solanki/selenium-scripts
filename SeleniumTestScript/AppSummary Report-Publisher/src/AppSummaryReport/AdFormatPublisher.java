package AppSummaryReport;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.naming.ConfigurationException;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.*;

import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;



  public class AdFormatPublisher {
  private WebDriver driver;
  private String baseUrl;
  private StringBuffer verificationErrors = new StringBuffer();
  private WebElement table2;
  private String impressions;
  private String revenue;
  private WebElement table;
  private WebElement table1;
  static PropertiesConfiguration configuration;
  private String publisher;
  private String pubEmail;
  private String base;
  private String query;
private String publisherId;
private String pubId;
	static {
		final String PROPERTIES_FILE_NAME = "C:\\AppSummary Report-Publisher\\config\\configurtion.properties";
		configuration = new PropertiesConfiguration();
		try {
			configuration.load(PROPERTIES_FILE_NAME);
		} catch (org.apache.commons.configuration.ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	/**
	 * @return the configuration
	 */
	public static PropertiesConfiguration getConfiguration() {
		return configuration;
	}

	/**
	 * @param configuration the configuration to set
	 */
	public static void setConfiguration(PropertiesConfiguration configuration) {
		AdFormatPublisher.configuration = configuration;
	}

	/**
	 * @return the driver
	 */
	public WebDriver getDriver() {
		return driver;
	}

	/**
	 * @param driver the driver to set
	 */
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	/**
	 * @return the baseUrl
	 */
	public String getBaseUrl() {
		return baseUrl;
	}

	/**
	 * @param baseUrl the baseUrl to set
	 */
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	/**
	 * @return the verificationErrors
	 */
	public StringBuffer getVerificationErrors() {
		return verificationErrors;
	}

	/**
	 * @param verificationErrors the verificationErrors to set
	 */
	public void setVerificationErrors(StringBuffer verificationErrors) {
		this.verificationErrors = verificationErrors;
	}

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    System.setProperty("webdriver.firefox.bin",
			(String) configuration.getProperty("webdriver.firefox.bin"));
    base = (String) configuration.getProperty("base.url");
    baseUrl = base;
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }


  @Test
  public void AdFormat() throws Exception {
    driver.get(baseUrl + "/vadmins/?page=login");
    driver.findElement(By.id("googleLogin")).click();
    String user = (String) configuration.getProperty("gmail.name");
	String Pass = (String) configuration.getProperty("gmail.password");
    driver.findElement(By.id("Email")).sendKeys(user);
    driver.findElement(By.id("Passwd")).clear();
    driver.findElement(By.id("Passwd")).sendKeys(Pass);
    driver.findElement(By.id("signIn")).click();
    driver.findElement(By.linkText("Publishers")).click();
    driver.findElement(By.id("searchtext")).clear();
    publisher = (String) configuration.getProperty("publisher.name");
    pubEmail = (String) configuration.getProperty("publisher.Email");
    driver.findElement(By.id("searchtext")).sendKeys(publisher);
    driver.findElement(By.name("action_search")).click();
    driver.findElement(By.linkText(pubEmail)).click();
    for(String winHandle : driver.getWindowHandles()) 
    {
        driver.switchTo().window(winHandle);
    }
    
    driver.findElement(By.linkText("Analytics")).click();
    new Select(driver.findElement(By.id("reporttype"))).selectByVisibleText("Ad Format Summary Report");
    //driver.findElement(By.id("start_date")).click();
    driver.findElement(By.id("start_date")).clear();
    String startdate = (String) configuration.getProperty("start.date");
    driver.findElement(By.id("start_date")).sendKeys(startdate);
    driver.findElement(By.id("start_date")).sendKeys(Keys.ENTER);
    /*driver.findElement(By.id("dp-nav-prev-month")).click();
    driver.findElement(By.xpath("//div[@id='dp-calendar']/table/tbody/tr/td[7]")).click();
    driver.findElement(By.id("end_date")).click();
    driver.findElement(By.id("dp-nav-prev-month")).click();
    driver.findElement(By.xpath("//div[@id='dp-calendar']/table/tbody/tr[6]/td")).click();*/
    driver.findElement(By.id("end_date")).clear();
    String Enddate = (String) configuration.getProperty("end.date");
    driver.findElement(By.id("end_date")).sendKeys(Enddate);
    driver.findElement(By.id("end_date")).sendKeys(Keys.ENTER);
    driver.findElement(By.name("submit")).click();
    table = driver.findElement(By.id("approvaltable"));
    impressions = driver.findElement(By.xpath("//table[@id='approvaltable']/tbody/tr[last()]/td[2]")).getText();
    System.out.print(impressions);
    String clicks = driver.findElement(By.xpath("//table[@id='approvaltable']/tbody/tr[last()]/td[3]")).getText();
    System.out.print(clicks);
    revenue = driver.findElement(By.xpath("//table[@id='approvaltable']/tbody/tr[last()]/td[5]")).getText();
    System.out.print(revenue);
    
    String url = (String) configuration.getProperty("url.name");
	getDriver().get(url);
    //driver.get("https://www.vdopia.com/vadmins");
      driver.findElement(By.id("googleLogin")).click();
      /*driver.findElement(By.id("Email")).clear();
      driver.findElement(By.id("Email")).sendKeys("saurabh.solanki@vdopia.com");
      driver.findElement(By.id("Passwd")).clear();
      driver.findElement(By.id("Passwd")).sendKeys("sau123456");
      driver.findElement(By.id("signIn")).click();*/
      driver.findElement(By.linkText("Tools")).click();
      /*driver.findElement(By.id("googleLogin")).click();
      driver.findElement(By.linkText("Tools")).click();*/
      driver.findElement(By.linkText("Query Browser Report")).click();
      driver.findElement(By.name("query")).click();
      driver.findElement(By.name("query")).clear();
      publisherId = "select * from publisher where email ='" +  pubEmail + "';";
      //String[] query = (String[]) configuration.getProperty("publisher.query");
      driver.findElement(By.name("query")).sendKeys(publisherId);
      driver.findElement(By.name("runsubmit")).click();
      driver.findElement(By.name("query")).click();
      driver.findElement(By.name("query")).clear();
      publisherId = "select * from publisher where email ='" +  pubEmail + "';";
      //String[] query = (String[]) configuration.getProperty("publisher.query");
      driver.findElement(By.name("query")).sendKeys(publisherId);
      driver.findElement(By.name("runsubmit")).click();
      table2 = driver.findElement(By.id("fi-approvaltable"));
      pubId = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[2])[2]")).getText();
      String  impressions1 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[2])[2]")).getText();
      System.out.print(impressions1);
      String clicks1 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[3])[2]")).getText();
      System.out.print(clicks1);
      String revenue1 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[10])[2]")).getText();
      System.out.print(revenue1);
      //Assert.assertEquals(impressions,impressions1);
      //System.out.print("Impressions are correct");
      if (impressions.equals(impressions1))
      {
          System.out.print("Impressions are correct");
      }
      else
      {
    	  System.out.print("Impressions are not correct");
      }
      
      if (clicks.equals(clicks1))
      {
          System.out.print("Clicks are correct");
      }
      else
      {
    	  System.out.print("Clicks are not correct");
      }
      if (revenue.equals(revenue1))
      {
          System.out.print("Revenue are correct");
      }
      else
      {
    	  System.out.print("Revenue are not correct");
      }
  
      // Pub Earning Report
      driver.findElement(By.name("query")).clear();
      String Pubquery = (String) configuration.getProperty("pub.query");
      driver.findElement(By.name("query")).sendKeys(Pubquery);
      driver.findElement(By.name("runsubmit")).click();
      table1 = driver.findElement(By.id("fi-approvaltable"));
      String  impressions2 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[1])[2]")).getText();
      System.out.print(impressions1);
      String clicks2 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[2])[2]")).getText();
      System.out.print(clicks1);
      String revenue2 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[5])[2]")).getText();
      System.out.print(revenue1);
      //Assert.assertEquals(impressions,impressions1);
      //System.out.print("Impressions are correct");
      if (impressions.equals(impressions2))
      {
          System.out.print("Impressions are correct");
      }
      else
      {
    	  System.out.print("Impressions are not correct");
      }
      
      if (clicks.equals(clicks2))
      {
          System.out.print("Clicks are correct");
      }
      else
      {
    	  System.out.print("Clicks are not correct");
      }
      if (revenue.equals(revenue2))
      {
          System.out.print("Revenue are correct");
      }
      else
      {
    	  System.out.print("Revenue are not correct");
      }
  }
  
  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

public WebElement getTable2() {
	return table2;
}

public void setTable2(WebElement table2) {
	this.table2 = table2;
}
}
