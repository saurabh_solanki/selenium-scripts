package AppSummaryReport;
import java.util.concurrent.TimeUnit;

import org.junit.*;

import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class CreativePerformancePublisher {
  private WebDriver driver;
  private String baseUrl;
  private StringBuffer verificationErrors = new StringBuffer();
  private WebElement  table;
  private WebElement table1;

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://mobile.vdopia.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testCreativePerformancePublisher() throws Exception {
	    driver.get(baseUrl + "/vadmins/?page=login");
	    driver.findElement(By.id("googleLogin")).click();
	    driver.findElement(By.id("Email")).clear();
	    driver.findElement(By.id("Email")).sendKeys("saurabh.solanki@vdopia.com");
	    driver.findElement(By.id("Passwd")).clear();
	    driver.findElement(By.id("Passwd")).sendKeys("sau123456");
	    driver.findElement(By.id("signIn")).click();
	    driver.findElement(By.linkText("Advertisers")).click();
	    driver.findElement(By.id("searchtext")).clear();
	    driver.findElement(By.id("searchtext")).sendKeys("srikantk");
	    driver.findElement(By.name("action_search")).click();
	    driver.findElement(By.linkText("srikantk@vdopia.com")).click();
	    for(String winHandle : driver.getWindowHandles()) 
	    {
	        driver.switchTo().window(winHandle);
	    }
	    
    driver.findElement(By.linkText("Reports")).click();
    new Select(driver.findElement(By.id("reporttype"))).selectByVisibleText("Creative Performance Report");
    driver.findElement(By.id("daterange")).clear();
    driver.findElement(By.id("daterange")).sendKeys("01-Sep-2013 to 30-Sep-2013");
    driver.findElement(By.id("daterange")).sendKeys(Keys.ENTER);
    driver.findElement(By.id("run")).click();
    table = driver.findElement(By.id("advreoprt"));
    String impressions = driver.findElement(By.xpath("//table[@id='advreoprt']/tfoot/tr[last()]/td[2]/b")).getText();
    System.out.print(impressions);
    String clicks = driver.findElement(By.xpath("//table[@id='advreoprt']/tfoot/tr[last()]/td[3]/b")).getText();
    System.out.print(clicks);
    String revenue = driver.findElement(By.xpath("//table[@id='advreoprt']/tfoot/tr[last()]/td[5]/b")).getText();
    System.out.print(revenue);
    for(String winHandle : driver.getWindowHandles()) 
    {
        driver.switchTo().window(winHandle);
    }
    driver.get("https://www.vdopia.com/vadmins");
    driver.findElement(By.id("googleLogin")).click();
      /*driver.findElement(By.id("Email")).clear();
      driver.findElement(By.id("Email")).sendKeys("saurabh.solanki@vdopia.com");
      driver.findElement(By.id("Passwd")).clear();
      driver.findElement(By.id("Passwd")).sendKeys("sau123456");
      driver.findElement(By.id("signIn")).click();*/
      driver.findElement(By.linkText("Tools")).click();
      /*driver.findElement(By.id("googleLogin")).click();
      driver.findElement(By.linkText("Tools")).click();*/
      driver.findElement(By.linkText("Query Browser Report")).click();
      driver.findElement(By.name("query")).click();
      driver.findElement(By.name("query")).clear();
      driver.findElement(By.name("query")).sendKeys("select if(ads.branded_img_right_ref_imgname IS NOT NULL,ads.branded_img_right_ref_imgname,'Others' ) as Creative,campaign.name ,sum(impressions) as impression,\nsum(clicks) as clicks,round(sum(clicks)/sum(impressions)*100,2) as ctr, sum(leads) as leads ,sum(completes) as completes,round(sum(completes)/sum(impressions)*100,2) as cr,\nround(sum(revenue_advcurrency),2) as advrevenue , round((sum(revenue_advcurrency)/sum(impressions)*1000),2) as cpm,\n\nsum(if(cp1.channel_id is NULL,if(cp.channel_id is NULL, pubrevenue_advcurrency,if(cp.model='CPM',\nimpressions*cp.price_per_unit*((select ex_rate from currency where code=advertisersheet.currency and updated=ds.date limit 1)/(select ex_rate from currency where code=publisher.currency and updated=ds.date limit 1)),\nif(cp.model='CPC',clicks*cp.price_per_unit*((select ex_rate from currency where code=advertisersheet.currency and updated=ds.date limit 1)/(select ex_rate from currency where code=publisher.currency and updated=ds.date limit 1)),0))),\nif(cp1.model='CPM',impressions*cp1.price_per_unit*((select ex_rate from currency where code=advertisersheet.currency and updated=ds.date limit 1)/(select ex_rate from currency where code=publisher.currency and updated=ds.date limit 1)),\nif(cp1.model='CPC', clicks*cp1.price_per_unit*((select ex_rate from currency where code=advertisersheet.currency and updated=ds.date limit 1)/(select ex_rate from currency where code=publisher.currency and updated=ds.date limit 1)),0))))\nas pubrev ,\n\nround(sum(if(cp1.channel_id is NULL,if(cp.channel_id is NULL, pubrevenue_advcurrency,if(cp.model='CPM',\nimpressions*cp.price_per_unit*((select ex_rate from currency where code=advertisersheet.currency and updated=ds.date limit 1)/(select ex_rate from currency where code=publisher.currency and updated=ds.date limit 1)),\nif(cp.model='CPC',clicks*cp.price_per_unit*((select ex_rate from currency where code=advertisersheet.currency and updated=ds.date limit 1)/(select ex_rate from currency where code=publisher.currency and updated=ds.date limit 1)),0))),\nif(cp1.model='CPM',impressions*cp1.price_per_unit*((select ex_rate from currency where code=advertisersheet.currency and updated=ds.date limit 1)/(select ex_rate from currency where code=publisher.currency and updated=ds.date limit 1)),\nif(cp1.model='CPC', clicks*cp1.price_per_unit*((select ex_rate from currency where code=advertisersheet.currency and updated=ds.date limit 1)/(select ex_rate from currency where code=publisher.currency and updated=ds.date limit 1)),0))))\n/sum(impressions)*1000,2) as ecpm \n\nfrom \n\ndaily_stats ds \nleft join campaign on (campaign.id=ds.campaign_id) \nleft join channels on (channels.id=ds.channel_id)\nleft join publisher on (publisher.id=channels.publisher_id )\nleft join advertisersheet on (advertisersheet.advertiser_id=campaign.advertiser_id )\nleft join ads on(ads.id=ds.ad_id)\nleft join (select * from price_config pc inner join price_config_member pcm on(pc.id=pcm.price_config_id)) as cp \non (ds.adtype=cp.adtype and cp.ccode='0' and ds.channel_id=cp.channel_id and ds.date>=cp.start_date and ds.date<=cp.end_date ) \nleft join (select * from price_config pc inner join price_config_member pcm on(pc.id=pcm.price_config_id)) as cp1 \non (cp1.ccode=ds.ccode and ds.adtype=cp1.adtype and cp1.channel_id=ds.channel_id and (ds.date>=cp1.start_date and ds.date<=cp1.end_date))\n\nwhere \nds.date>='2013-09-01' and ds.date<='2013-09-30' and campaign.advertiser_id=185 and campaign.device='iphone';");
      driver.findElement(By.name("runsubmit")).click();
      table1 = driver.findElement(By.id("fi-approvaltable"));
      String  impressions1 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[2])[2]")).getText();
      System.out.print(impressions1);
      String clicks1 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[3])[2]")).getText();
      System.out.print(clicks1);
      String revenue1 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[10])[2]")).getText();
      System.out.print(revenue1);
      //Assert.assertEquals(impressions,impressions1);
      //System.out.print("Impressions are correct");
      if (impressions.equals(impressions1))
      {
          System.out.print("Impressions are correct");
      }
      else
      {
    	  System.out.print("Impressions are not correct");
      }
      
      if (clicks.equals(clicks1))
      {
          System.out.print("Clicks are correct");
      }
      else
      {
    	  System.out.print("Clicks are not correct");
      }
      if (revenue.equals(revenue1))
      {
          System.out.print("Revenue are correct");
      }
      else
      {
    	  System.out.print("Revenue are not correct");
      }
  
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }
}

