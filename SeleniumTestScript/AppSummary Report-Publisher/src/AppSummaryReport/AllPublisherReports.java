package AppSummaryReport;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

  public class AllPublisherReports {
  private WebDriver driver;
  private String baseUrl;
  private StringBuffer verificationErrors = new StringBuffer();
  private WebElement row;
  private WebElement table2;
  private WebElement element;
  private List<WebElement> secondColumns;
  private String impressions;
  private String revenue;
private WebElement table;
private WebElement table1;
private WebElement appSummarytable;
private WebElement countrySummarytable;
private WebElement adFormattable;

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://mobile.vdopia.com";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testAllPublisherReports() throws Exception {
    driver.get(baseUrl + "/vadmins/?page=login");
    driver.findElement(By.id("googleLogin")).click();
    driver.findElement(By.id("Email")).clear();
    driver.findElement(By.id("Email")).sendKeys("saurabh.solanki@vdopia.com");
    driver.findElement(By.id("Passwd")).clear();
    driver.findElement(By.id("Passwd")).sendKeys("sau123456");
    driver.findElement(By.id("signIn")).click();
    driver.findElement(By.linkText("Publishers")).click();
    driver.findElement(By.id("searchtext")).clear();
    driver.findElement(By.id("searchtext")).sendKeys("kevin");
    driver.findElement(By.name("action_search")).click();
    driver.findElement(By.linkText("kevin@kargo.com")).click();
    for(String winHandle : driver.getWindowHandles()) 
    {
        driver.switchTo().window(winHandle);
    }
    //Campaign Summary Report
    driver.findElement(By.linkText("Analytics")).click();
    new Select(driver.findElement(By.id("reporttype"))).selectByVisibleText("Campaign Summary Report");
    driver.findElement(By.id("start_date")).clear();
    driver.findElement(By.id("start_date")).sendKeys("2013-09-01");
    driver.findElement(By.id("start_date")).sendKeys(Keys.ENTER);
   // driver.findElement(By.id("start_date")).sendKeys(Keys.ENTER);
    /*WebElement dateWidget = driver.findElement(By.id("dp-calendar"));  
    List<webelement> rows=dateWidget.findElements(By.tagName("tr"));  
    List<webelement> columns=dateWidget.findElements(By.tagName("td"));  
      
    for (WebElement cell: columns){  
     //Select 13th Date   
     if (cell.getText().equals("13")){  
     cell.findElement(By.linkText("13")).click();  
     break;  
     }  
    }   
     */
   // driver.findElement(By.xpath("//div[@id='dp-calendar']/table/tbody/tr/td[7]")).click();
    driver.findElement(By.id("end_date")).clear();
    driver.findElement(By.id("end_date")).sendKeys("2013-09-30");
    driver.findElement(By.id("end_date")).sendKeys(Keys.ENTER);
    //driver.findElement(By.id("start_date")).sendKeys(Keys.ENTER);
    //driver.findElement(By.id("dp-nav-prev-month")).click();
   // driver.findElement(By.xpath("//div[@id='dp-calendar']/table/tbody/tr[6]/td")).click();
    driver.findElement(By.name("submit")).click();
    table = driver.findElement(By.id("approvaltable"));
    impressions = driver.findElement(By.xpath("//table[@id='approvaltable']/tbody/tr[last()]/td[2]")).getText();
    System.out.print(impressions);
    String clicks = driver.findElement(By.xpath("//table[@id='approvaltable']/tbody/tr[last()]/td[3]")).getText();
    System.out.print(clicks);
    revenue = driver.findElement(By.xpath("//table[@id='approvaltable']/tbody/tr[last()]/td[5]")).getText();
    System.out.print(revenue);
    
   /* driver.get("https://www.vdopia.com/vadmins");
    driver.findElement(By.id("googleLogin")).click();
      driver.findElement(By.id("Email")).clear();
      driver.findElement(By.id("Email")).sendKeys("saurabh.solanki@vdopia.com");
      driver.findElement(By.id("Passwd")).clear();
      driver.findElement(By.id("Passwd")).sendKeys("sau123456");
      driver.findElement(By.id("signIn")).click();
      driver.findElement(By.linkText("Tools")).click();
      driver.findElement(By.id("googleLogin")).click();
      driver.findElement(By.linkText("Tools")).click();
      driver.findElement(By.linkText("Query Browser Report")).click();
      driver.findElement(By.name("query")).click();
      driver.findElement(By.name("query")).clear();
      driver.findElement(By.name("query")).sendKeys("select campaign.name,sum(impressions) as impression,sum(clicks) as clicks,round(sum(clicks)/sum(impressions)*100,2)  as ctr, sum(leads) as leads ,\nsum(completes) as completes,round(sum(completes)/sum(impressions)*100,2)  as cr, round(sum(revenue_advcurrency*ex_rate_advpub),2) as advrevenue , \nround((sum(revenue_advcurrency*ex_rate_advpub)/sum(impressions)*1000),2) as cpm,\nsum(if(cp1.channel_id is null, if(cp.id is null,pubrevenue_advcurrency*ex_rate_advpub,if(cp.model='CPM',\nimpressions*cp.price_per_unit, if(ads.parent_ad_id is null,ds.clicks,0)*cp.price_per_unit)),\nif(cp1.model='CPM', impressions*cp1.price_per_unit, if(ads.parent_ad_id is\nnull,ds.clicks,0)*cp1.price_per_unit))) as pubrev ,\n\nround(sum(if(cp1.channel_id is null, if(cp.id is null,pubrevenue_advcurrency*ex_rate_advpub,if(cp.model='CPM',\nimpressions*cp.price_per_unit, if(ads.parent_ad_id is null,ds.clicks,0)*cp.price_per_unit)),\nif(cp1.model='CPM', impressions*cp1.price_per_unit, if(ads.parent_ad_id is\nnull,ds.clicks,0)*cp1.price_per_unit)))*1000/sum(impressions),2) as ecpm \nfrom \ndaily_stats ds \nleft join channels on (channels.id=ds.channel_id) \nleft join campaign on (campaign.id=ds.campaign_id) \nleft join ads on(ds.ad_id=ads.id) \nleft join (select * from price_config pc inner join price_config_member pcm on(pc.id=pcm.price_config_id)) as cp \non (ds.adtype=cp.adtype and cp.ccode='0' and ds.channel_id=cp.channel_id and ds.date>=cp.start_date and ds.date<=cp.end_date) \nleft join (select * from price_config pc inner join price_config_member pcm on(pc.id=pcm.price_config_id)) as cp1 \non (cp1.ccode=ds.ccode and ds.adtype=cp1.adtype and cp1.channel_id=ds.channel_id and (ds.date>=cp1.start_date and ds.date<=cp1.end_date))\n\nwhere \nds.date>='2013-09-01' and ds.date<='2013-09-30' and channels.publisher_id=1984   and channels.channel_type='iphone';");
      driver.findElement(By.name("runsubmit")).click();
      table1 = driver.findElement(By.id("fi-approvaltable"));
      String  impressions1 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[2])[2]")).getText();
      System.out.print(impressions1);
      String clicks1 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[3])[2]")).getText();
      System.out.print(clicks1);
      String revenue1 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[10])[2]")).getText();
      System.out.print(revenue1);
      //Assert.assertEquals(impressions,impressions1);
      //System.out.print("Impressions are correct");
      if (impressions.equals(impressions1))
      {
          System.out.print("Impressions are correct");
      }
      else
      {
    	  System.out.print("Impressions are not correct");
      }
      
      if (clicks.equals(clicks1))
      {
          System.out.print("Clicks are correct");
      }
      else
      {
    	  System.out.print("Clicks are not correct");
      }
      if (revenue.equals(revenue1))
      {
          System.out.print("Revenue are correct");
      }
      else
      {
    	  System.out.print("Revenue are not correct");
    	  
      }
      //driver.findElement(By.linkText("Logout")).click();
      for(String winHandle : driver.getWindowHandles()) 
      {
          driver.switchTo().window(winHandle);
      }*/
     
      //App Summary Report
  new Select(driver.findElement(By.id("reporttype"))).selectByVisibleText("App Summary Report");
  driver.findElement(By.name("submit")).click();
  appSummarytable = driver.findElement(By.id("approvaltable"));
  String AppSummaryimpressions = driver.findElement(By.xpath("//table[@id='approvaltable']/tbody/tr[last()]/td[2]")).getText();
  System.out.print(AppSummaryimpressions);
  String AppSummaryclicks = driver.findElement(By.xpath("//table[@id='approvaltable']/tbody/tr[last()]/td[3]")).getText();
  System.out.print(AppSummaryclicks);
  String AppSummaryrevenue = driver.findElement(By.xpath("//table[@id='approvaltable']/tbody/tr[last()]/td[5]")).getText();
  System.out.print(AppSummaryrevenue);
  if (impressions.equals(AppSummaryimpressions))
  {
      System.out.print("Impressions are matched");
  }
  else
  {
	  System.out.print("Impressions are not matched");
  }
  
  if (clicks.equals(AppSummaryclicks))
  {
      System.out.print("Clicks are matched");
  }
  else
  {
	  System.out.print("Clicks are not matched");
  }
  if (revenue.equals(AppSummaryrevenue))
  {
      System.out.print("Revenue are matched");
  }
  else
  {
	  System.out.print("Revenue are not matched");
  }
  
  // Country Summary Report
  new Select(driver.findElement(By.id("reporttype"))).selectByVisibleText("Country Summary Report");
  driver.findElement(By.name("submit")).click();
  countrySummarytable = driver.findElement(By.id("approvaltable"));
  String CountrySummaryimpressions = driver.findElement(By.xpath("//table[@id='approvaltable']/tbody/tr[last()]/td[2]")).getText();
  System.out.print(CountrySummaryimpressions);
  String CountrySummaryclicks = driver.findElement(By.xpath("//table[@id='approvaltable']/tbody/tr[last()]/td[3]")).getText();
  System.out.print(CountrySummaryclicks);
  String CountrySummaryrevenue = driver.findElement(By.xpath("//table[@id='approvaltable']/tbody/tr[last()]/td[5]")).getText();
  System.out.print(CountrySummaryrevenue);
  if (impressions.equals(CountrySummaryimpressions))
  {
      System.out.print("Impressions are matched");
  }
  else
  {
	  System.out.print("Impressions are not matched");
  }
  
  if (clicks.equals(CountrySummaryclicks))
  {
      System.out.print("Clicks are matched");
  }
  else
  {
	  System.out.print("Clicks are not matched");
  }
  if (revenue.equals(CountrySummaryrevenue))
  {
      System.out.print("Revenue are matched");
  }
  else
  {
	  System.out.print("Revenue are not matched");
  }
  
  //Ad Format Report
  
  
  new Select(driver.findElement(By.id("reporttype"))).selectByVisibleText("Ad Format Summary Report");
  adFormattable = driver.findElement(By.id("approvaltable"));
  String AdFormatimpressions = driver.findElement(By.xpath("//table[@id='approvaltable']/tbody/tr[last()]/td[2]")).getText();
  System.out.print(AdFormatimpressions);
  String AdFormatclicks = driver.findElement(By.xpath("//table[@id='approvaltable']/tbody/tr[last()]/td[3]")).getText();
  System.out.print(AdFormatclicks);
  String AdFormatrevenue = driver.findElement(By.xpath("//table[@id='approvaltable']/tbody/tr[last()]/td[5]")).getText();
  System.out.print(AdFormatrevenue);
  
  if (impressions.equals(AdFormatimpressions))
  {
      System.out.print("Impressions are matched");
  }
  else
  {
	  System.out.print("Impressions are not matched");
  }
  
  if (clicks.equals(AdFormatclicks))
  {
      System.out.print("Clicks are matched");
  }
  else
  {
	  System.out.print("Clicks are not matched");
  }
  if (revenue.equals(AdFormatrevenue))
  {
      System.out.print("Revenue are matched");
  }
  else
  {
	  System.out.print("Revenue are not matched");
  }
  }
  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

public WebElement getTable2() {
	return table2;
}

public void setTable2(WebElement table2) {
	this.table2 = table2;
}
}
