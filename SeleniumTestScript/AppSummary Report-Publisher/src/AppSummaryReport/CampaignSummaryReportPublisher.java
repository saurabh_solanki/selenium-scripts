package AppSummaryReport;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.*;

import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

  public class CampaignSummaryReportPublisher {
  private WebDriver driver;
  private String baseUrl;
  private StringBuffer verificationErrors = new StringBuffer();
  private WebElement row;
  private WebElement table2;
  private WebElement element;
  private List<WebElement> secondColumns;
  private String impressions;
  private String revenue;
  private WebElement table;
  private WebElement table1;
  private WebElement table22;


  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://mobile.vdopia.com";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testCampaignSummaryReportPublisher() throws Exception {
    driver.get(baseUrl + "/vadmins/?page=login");
    driver.findElement(By.id("googleLogin")).click();
    driver.findElement(By.id("Email")).clear();
    driver.findElement(By.id("Email")).sendKeys("saurabh.solanki@vdopia.com");
    driver.findElement(By.id("Passwd")).clear();
    driver.findElement(By.id("Passwd")).sendKeys("sau123456");
    driver.findElement(By.id("signIn")).click();
    driver.findElement(By.linkText("Publishers")).click();
    driver.findElement(By.id("searchtext")).clear();
    driver.findElement(By.id("searchtext")).sendKeys("kevin");
    driver.findElement(By.name("action_search")).click();
    driver.findElement(By.linkText("kevin@kargo.com")).click();
    for(String winHandle : driver.getWindowHandles()) 
    {
        driver.switchTo().window(winHandle);
    }
    
    driver.findElement(By.linkText("Analytics")).click();
    new Select(driver.findElement(By.id("reporttype"))).selectByVisibleText("Campaign Summary Report");
    driver.findElement(By.id("start_date")).clear();
    driver.findElement(By.id("start_date")).sendKeys("2013-09-01");
    driver.findElement(By.id("start_date")).sendKeys(Keys.ENTER);
    driver.findElement(By.id("end_date")).clear();
    driver.findElement(By.id("end_date")).sendKeys("2013-09-30");
    driver.findElement(By.id("end_date")).sendKeys(Keys.ENTER);
    driver.findElement(By.name("submit")).click();
    table = driver.findElement(By.id("approvaltable"));
    impressions = driver.findElement(By.xpath("//table[@id='approvaltable']/tbody/tr[last()]/td[2]")).getText();
    System.out.print(impressions);
    String clicks = driver.findElement(By.xpath("//table[@id='approvaltable']/tbody/tr[last()]/td[3]")).getText();
    System.out.print(clicks);
    revenue = driver.findElement(By.xpath("//table[@id='approvaltable']/tbody/tr[last()]/td[5]")).getText();
    System.out.print(revenue);
    
    driver.get("https://www.vdopia.com/vadmins");
    driver.findElement(By.id("googleLogin")).click();
      /*driver.findElement(By.id("Email")).clear();
      driver.findElement(By.id("Email")).sendKeys("saurabh.solanki@vdopia.com");
      driver.findElement(By.id("Passwd")).clear();
      driver.findElement(By.id("Passwd")).sendKeys("sau123456");
      driver.findElement(By.id("signIn")).click();*/
      driver.findElement(By.linkText("Tools")).click();
      driver.findElement(By.linkText("Query Browser Report")).click();
      driver.findElement(By.name("query")).click();
      driver.findElement(By.name("query")).clear();
      driver.findElement(By.name("query")).sendKeys("select campaign.name,sum(impressions) as impression,sum(clicks) as clicks,round(sum(clicks)/sum(impressions)*100,2)  as ctr, sum(leads) as leads ,\nsum(completes) as completes,round(sum(completes)/sum(impressions)*100,2)  as cr, round(sum(revenue_advcurrency*ex_rate_advpub),2) as advrevenue , \nround((sum(revenue_advcurrency*ex_rate_advpub)/sum(impressions)*1000),2) as cpm,\nsum(if(cp1.channel_id is null, if(cp.id is null,pubrevenue_advcurrency*ex_rate_advpub,if(cp.model='CPM',\nimpressions*cp.price_per_unit, if(ads.parent_ad_id is null,ds.clicks,0)*cp.price_per_unit)),\nif(cp1.model='CPM', impressions*cp1.price_per_unit, if(ads.parent_ad_id is\nnull,ds.clicks,0)*cp1.price_per_unit))) as pubrev ,\n\nround(sum(if(cp1.channel_id is null, if(cp.id is null,pubrevenue_advcurrency*ex_rate_advpub,if(cp.model='CPM',\nimpressions*cp.price_per_unit, if(ads.parent_ad_id is null,ds.clicks,0)*cp.price_per_unit)),\nif(cp1.model='CPM', impressions*cp1.price_per_unit, if(ads.parent_ad_id is\nnull,ds.clicks,0)*cp1.price_per_unit)))*1000/sum(impressions),2) as ecpm \nfrom \ndaily_stats ds \nleft join channels on (channels.id=ds.channel_id) \nleft join campaign on (campaign.id=ds.campaign_id) \nleft join ads on(ds.ad_id=ads.id) \nleft join (select * from price_config pc inner join price_config_member pcm on(pc.id=pcm.price_config_id)) as cp \non (ds.adtype=cp.adtype and cp.ccode='0' and ds.channel_id=cp.channel_id and ds.date>=cp.start_date and ds.date<=cp.end_date) \nleft join (select * from price_config pc inner join price_config_member pcm on(pc.id=pcm.price_config_id)) as cp1 \non (cp1.ccode=ds.ccode and ds.adtype=cp1.adtype and cp1.channel_id=ds.channel_id and (ds.date>=cp1.start_date and ds.date<=cp1.end_date))\n\nwhere \nds.date>='2013-09-01' and ds.date<='2013-09-30' and channels.publisher_id=1984   and channels.channel_type='iphone';");
      driver.findElement(By.name("runsubmit")).click();
      table1 = driver.findElement(By.id("fi-approvaltable"));
      String  impressions1 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[2])[2]")).getText();
      System.out.print(impressions1);
      String clicks1 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[3])[2]")).getText();
      System.out.print(clicks1);
      String revenue1 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[10])[2]")).getText();
      System.out.print(revenue1);
      //Assert.assertEquals(impressions,impressions1);
      //System.out.print("Impressions are correct");
      if (impressions.equals(impressions1))
      {
          System.out.print("Impressions are correct");
      }
      else
      {
    	  System.out.print("Impressions are not correct");
      }
      
      if (clicks.equals(clicks1))
      {
          System.out.print("Clicks are correct");
      }
      else
      {
    	  System.out.print("Clicks are not correct");
      }
      if (revenue.equals(revenue1))
      {
          System.out.print("Revenue are correct");
      }
      else
      {
    	  System.out.print("Revenue are not correct");
      }
      for(String winHandle : driver.getWindowHandles()) 
      {
          driver.switchTo().window(winHandle);
      }
      driver.get("https://www.vdopia.com/vadmins");
       // driver.findElement(By.id("googleLogin")).click();
        driver.findElement(By.linkText("Tools")).click();
        driver.findElement(By.linkText("Query Browser Report")).click();
        //driver.findElement(By.name("query")).click();
        driver.findElement(By.name("query")).clear();
        driver.findElement(By.name("query")).sendKeys("select sum(total_impressions), sum(total_clicks), sum(completes), sum(pub_revenue_without_channel_price_override) without , sum(pub_revenue) from pub_earnings_2013_09 where device='iphone' and channel_type='iphone' and pub_id=1984;");
        driver.findElement(By.name("runsubmit")).click();
        table22 = driver.findElement(By.id("fi-approvaltable"));
        String  impressions2 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[1])[2]")).getText();
        System.out.print(impressions2);
        String clicks2 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[2])[2]")).getText();
        System.out.print(clicks2);
        String revenue2 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[5])[2]")).getText();
        System.out.print(revenue2);
      //Assert.assertEquals(impressions,impressions1);
      //System.out.print("Impressions are correct");
      if (impressions.equals(impressions2))
      {
          System.out.print("Impressions are correct");
      }
      else
      {
    	  System.out.print("Impressions are not correct");
      }
      
      if (clicks.equals(clicks2))
      {
          System.out.print("Clicks are correct");
      }
      else
      {
    	  System.out.print("Clicks are not correct");
      }
      if (revenue.equals(revenue2))
      {
          System.out.print("Revenue are correct");
      }
      else
      {
    	  System.out.print("Revenue are not correct");
      }
  }
  
  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

public WebElement getTable2() {
	return table2;
}

public void setTable2(WebElement table2) {
	this.table2 = table2;
}
}
  
  
  