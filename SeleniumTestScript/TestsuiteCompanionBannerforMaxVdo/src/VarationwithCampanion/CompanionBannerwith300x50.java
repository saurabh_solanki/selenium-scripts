package VarationwithCampanion;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.assertEquals;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

@SuppressWarnings("unused")
public class CompanionBannerwith300x50 {

	static PropertiesConfiguration configuration;
	static {
		final String PROPERTIES_FILE_NAME = "C:\\Users\\Saurabh\\workspace\\VideoUs\\conf\\configuration.properties";
		configuration = new PropertiesConfiguration();
		try {
			configuration.load(PROPERTIES_FILE_NAME);
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}

	}
	/**
	 * @return the configuration
	 */
	public static PropertiesConfiguration getConfiguration() {
		return configuration;
	}

	/**
	 * @param configuration the configuration to set
	 */
	public static void setConfiguration(PropertiesConfiguration configuration) {
		CompanionBannerwith300x50.configuration = configuration;
	}

	/**
	 * @return the driver
	 */
	public WebDriver getDriver() {
		return driver;
	}

	/**
	 * @param driver the driver to set
	 */
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	/**
	 * @return the baseUrl
	 */
	public String getBaseUrl() {
		return baseUrl;
	}

	/**
	 * @param baseUrl the baseUrl to set
	 */
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	/**
	 * @return the verificationErrors
	 */
	public StringBuffer getVerificationErrors() {
		return verificationErrors;
	}

	/**
	 * @param verificationErrors the verificationErrors to set
	 */
	public void setVerificationErrors(StringBuffer verificationErrors) {
		this.verificationErrors = verificationErrors;
	}

	/**
	 * @return the result
	 */
	public static String getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public static void setResult(String result) {
		CompanionBannerwith300x50.result = result;
	}

	/**
	 * @return the campaign
	 */
	public String getCampaign() {
		return campaign;
	}

	/**
	 * @param campaign the campaign to set
	 */
	public void setCampaign(String campaign) {
		this.campaign = campaign;
	}

	/**
	 * @return the search
	 */
	public String getSearch() {
		return search;
	}

	/**
	 * @param search the search to set
	 */
	public void setSearch(String search) {
		this.search = search;
	}

	/**
	 * @return the finalDate
	 */
	public String getFinalDate() {
		return finalDate;
	}

	/**
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}

	private WebDriver driver;
	private String baseUrl;
	private StringBuffer verificationErrors = new StringBuffer();
	private static String result;
	private String campaign;
	public String search;
	public String finalDate;

	@BeforeTest
	public void setUp() throws Exception, ConfigurationException {

		FirefoxProfile firefoxProfile = new FirefoxProfile();
		firefoxProfile.setPreference("network.proxy.type", 1);
		firefoxProfile.setPreference("network.proxy.http", "losangeles.wonderproxy.com");
		firefoxProfile.setPreference("network.proxy.http_port", 80);
		firefoxProfile.setPreference("network.proxy.ssl", "losangeles.wonderproxy.com");
		firefoxProfile.setPreference("network.proxy.ssl_port", 80);
		firefoxProfile.setPreference("network.proxy.no_proxies_on", "");
		
		//Process P = Runtime.getRuntime().exec("C:\\Users\\Saurabh\\Desktop\\autoit script\\authentication.exe");
		driver = new FirefoxDriver(firefoxProfile);
		
		System.setProperty("webdriver.firefox.bin",
				(String) configuration.getProperty("webdriver.firefox.bin"));
		baseUrl = "https://mobile.sb.vdopia.com";
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	/**
	 * The method fetches the code from the configuration properties file placed
	 * at location derived from the variable PROPERTIES_FILE_NAME
	 * 
	 * @return
	 * @throws ConfigurationException
	 */
	// private String getPropertyFromConfigFile(String prop)
	//
	// throws ConfigurationException {
	// //PropertiesConfiguration configuration = new PropertiesConfiguration();
	// //configuration.load(PROPERTIES_FILE_NAME);
	// System.out.println(configuration.getProperty("webdriver.firefox.bin"));
	// return (String)configuration.getProperty(prop);
	// }
	
	
	
	@Test(priority = 0)
	public void GmailLogin() throws IOException {
		
		//Process P = Runtime.getRuntime().exec("C:\\Users\\Saurabh\\Desktop\\autoit script\\authentication.exe");
		setProcess(new ProcessBuilder(
				"C:\\Users\\Saurabh\\Desktop\\autoit script\\authentication.exe").start());
		String url = (String) configuration.getProperty("url.name");
		getDriver().get(getBaseUrl() + url);
		//Process P = Runtime.getRuntime().exec("C:\\Users\\Saurabh\\Desktop\\autoit script\\authentication.exe");
		//P.destroy();
		//setProcess(new ProcessBuilder(
				//"C:\\Users\\Saurabh\\Desktop\\autoit script\\authentication.exe").start());
		driver.findElement(By.id("googleLogin")).click();
		driver.findElement(By.id("Email")).clear();
		String user = (String) configuration.getProperty("gmail.name");
		String Pass = (String) configuration.getProperty("gmail.password");
		driver.findElement(By.id("Email")).sendKeys(user);
		driver.findElement(By.id("Passwd")).clear();
		driver.findElement(By.id("Passwd")).sendKeys(Pass);
		driver.findElement(By.id("signIn")).click();
	}

	@Test(priority = 1)
	public void PublisherSearch() {
		driver.findElement(By.linkText("Publishers")).click();
		driver.findElement(By.id("searchtext")).sendKeys("saurabh");
		driver.findElement(By.name("action_search")).click();
		driver.findElement(By.linkText("saurabh.solanki@vdopia.com")).click();
		driver.close();
	}

	@Test(priority = 2)
	public void ChannelCreation() {
		
		//Declarations
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd:HH:mm");
		Date date = new Date();
		finalDate = dateFormat.format(date);
		
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		driver.findElement(By.cssSelector("b")).click();
		// System.out.print(FinalDate);
		driver.findElement(By.id("name")).clear();
		;
		
		driver.findElement(By.id("name")).sendKeys("App on" + (finalDate));
		driver.findElement(By.id("app_type_web")).click();
		driver.findElement(By.id("url")).clear();
		driver.findElement(By.id("url")).sendKeys("www.tester.com");
		//driver.findElement(By.id("BO")).click();
		//assertEquals("BO", driver.findElement(By.id("BO")).getAttribute("value"));
		driver.findElement(By.id("submit")).click();
		driver.findElement(
				By.xpath("(//a[contains(text(),'Test Mode')])[last()]"))
				.click();
		driver.findElement(
				By.xpath("/html/body/div/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[last()]/td[2]/div/input"))
				.click();
		search = driver
				.findElement(
						By.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[4]"))
				.getText();
		System.out.print(search);
	}

	@Test(priority = 3)
	public void Advertiser() throws IOException {
		String url2 = (String) configuration.getProperty("url.name2");
		getDriver().get(getBaseUrl() + url2);
		driver.findElement(By.linkText("Advertisers")).click();
		driver.findElement(By.id("searchtext")).sendKeys("saurabh");
		driver.findElement(By.name("action_search")).click();
		driver.findElement(By.linkText("saurabh.solanki@vdopia.com")).click();
		driver.close();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		System.out.println("Title of the page after - switchingTo: "
				+ driver.getTitle());
	}

	@Test(priority = 4)
	public void CreateVideo() throws InterruptedException, IOException {
		driver.findElement(By.xpath("//input[@value='Create New Ad Campaign']"))
				.click();
		driver.findElement(By.id("videoadvertisement")).click();
		driver.findElement(By.cssSelector("input.submit")).click();
		Thread.sleep(Long.valueOf((String) configuration
				.getProperty("thread.wait.time1")));
		//Thread.sleep(5000);
		// driver.findElement(By.id("movie_player-0")).click();
		setProcess(new ProcessBuilder(
				"C:\\Users\\Saurabh\\Desktop\\autoit script\\video.exe",
				"C:\\Users\\Saurabh\\Desktop\\15 sec less than 6mb mpeg.mpg",
				"Open").start());
		
		Thread.sleep(Long.valueOf((String) configuration
				.getProperty("thread.wait.time2")));

		driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);
		driver.findElement(By.id("name")).sendKeys("test u");
		driver.findElement(By.cssSelector("#weburl > img")).click();
		driver.findElement(By.id("desturl")).sendKeys("http://m.aol.com");
		driver.findElement(By.id("saveButtom")).click();
		driver.findElement(By.xpath("//input[@value='Save & Continue >>']"))
				.click();
		
		driver.findElement(By.id("imagecompanion")).click();
		driver.findElement(By.cssSelector("input.submit")).click();
		setProcess(new ProcessBuilder(
				"C:\\Users\\Saurabh\\Desktop\\autoit script\\campaninbanner.exe",
				"C:\\Users\\Saurabh\\Desktop\\braced-bending-dvd-banner-300x60.png",
				"Open").start());
		Thread.sleep(50000);
	    new Select(driver.findElement(By.id("adname"))).selectByVisibleText("test u");
	    new Select(driver.findElement(By.id("bannersize"))).selectByVisibleText("300x60");
	    driver.findElement(By.id("saveButton")).click();
	    driver.findElement(By.xpath("//input[@value='Next']")).click();
	    driver.findElement(By.cssSelector("input.submit")).click();	

	}

	private void setProcess(Process start) {
		// TODO Auto-generated method stub

	}

	@Test(priority = 5)
	public void Targeting() throws InterruptedException {
		//driver.findElement(By.id("choosecategory")).click();
	    //driver.findElement(By.id("categoriesselected18")).click();
	   // assertEquals("BO", driver.findElement(By.id("categoriesselected18")).getAttribute("value"));
		driver.findElement(By.id("choosegeotargeting")).click();
		driver.findElement(By.id("cntUS")).click();
		driver.findElement(By.id("choosesitetargeting")).click();
		driver.findElement(By.id("sss")).sendKeys((search));
	    Thread.sleep(20000);
	    driver.findElement(By.id("site1")).sendKeys((search));
	    driver.findElement(By.xpath("//input[@value=' Add>>     ']")).click();
	    Thread.sleep(20000);
		driver.findElement(By.id("submit")).click();
	}

	@Test(priority = 6)
	public void Buget() {
		new Select(driver.findElement(By.id("model")))
				.selectByVisibleText("CPCV");
		driver.findElement(By.id("maxcpm")).clear();
		driver.findElement(By.id("maxcpm")).sendKeys("1");
		driver.findElement(By.id("dailylimit")).clear();
		driver.findElement(By.id("dailylimit")).sendKeys("12");
		driver.findElement(By.id("budget")).clear();
		driver.findElement(By.id("budget")).sendKeys("122");
		new Select(driver.findElement(By.id("priority_level")))
				.selectByVisibleText("Backfill");
		new Select(driver.findElement(By.name("pacing_type")))
				.selectByVisibleText("Uniform");
		new Select(driver.findElement(By.name("timezone")))
				.selectByVisibleText("IST (Indian Standard Time)");
		driver.findElement(By.id("start_date")).click();
		driver.findElement(
				By.xpath("//div[@id='dp-calendar']/table/tbody/tr[3]/td[4]"))
				.click();
		// WebElement dateWidget = driver.findElement(By.id("dp-calendar"));
		// List<WebElement> columns=dateWidget.findElements(By.tagName("td"));

		// for (WebElement cell : columns) {

		// String today = dateFormat2.format(date2);
		// if (cell.getText().equals(today)) {
		// cell.click();
		// break;
		// }

		// }
		driver.findElement(By.id("end_date")).click();
		driver.findElement(
				By.xpath("//div[@id='dp-calendar']/table/tbody/tr[4]/td[3]"))
				.click();
		driver.findElement(By.id("frequencyperday")).clear();
		driver.findElement(By.id("frequencyperday")).sendKeys("5");
		driver.findElement(By.id("frequencyperweek")).clear();
		driver.findElement(By.id("frequencyperweek")).sendKeys("35");
		driver.findElement(By.id("frequencypermonth")).clear();
		driver.findElement(By.id("frequencypermonth")).sendKeys("210");
		driver.findElement(By.id("action_submit")).click();
	}

	/**
	     * 
	     */
	@Test(priority = 7)
	public void CampaignCreation() {
		driver.findElement(By.id("campaignname")).sendKeys(
				"Video on " + (finalDate));
		campaign = driver.findElement(By.id("campaignname")).getAttribute(
				"value");
		System.out.println(campaign);
		driver.findElement(By.id("submit")).click();
		Alert javascriptAlert = driver.switchTo().alert();
	    System.out.println(javascriptAlert.getText()); // Get text on alert box
	    javascriptAlert.accept();
		driver.findElement(By.name("action_approve")).click();
	}

	// Connecting to Databse in order to Select Campaign Id
	/**
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 */
	@Test(priority = 8)
	public void Memcamp() throws InterruptedException, IOException {

		// String driver = props.getProperty("jdbc.driver");
		String url =(String) configuration.getProperty("jdbc.url");
		String username = (String) configuration.getProperty("jdbc.username");
		String password = (String) configuration.getProperty("jdbc.password");
		String dbClass = (String) configuration.getProperty("jdb.class");

		/*
		 * String dbUrl = "jdbc:mysql://sb.vdopia.com:3306/adplatform"; String
		 * username="saurabh"; String password="saurabh4114VDO"; String dbClass
		 * = "com.mysql.jdbc.Driver"; String query =
		 * "select id from campaign where name = ''+ 'campaign'";
		 */
		String query = "select id from campaign where name = ''+ 'campaign'";

		try {

			Class.forName(dbClass);
			// Connection con = DriverManager.getConnection
			// (dbUrl,username,password);
			Connection con = DriverManager.getConnection(url, username,
					password);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnsNumber = rsmd.getColumnCount();

			while (rs.next()) {
				// Print one row
				for (int i2 = 1; i2 <= columnsNumber; i2++) {
					result = rs.getString(i2) + " ";
					// System.out.println(result);
					// System.out.println();//Move to the next line to print the
					// next row.

				} // end while
			}
			con.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// Checking Entry In Memcamp
		String url3 = (String) configuration.getProperty("url.name3");
		getDriver().get(url3);
		//driver.get(baseUrl + url3);
		Thread.sleep(Long.valueOf((String) configuration
				.getProperty("thread.wait.time3")));
		driver.findElement(By.id("cid")).sendKeys((result));
		driver.findElement(By.name("submit")).click();
		try {
			AssertJUnit.assertTrue(driver.findElement(By.cssSelector("BODY"))
					.getText()
					.matches("^[\\s\\S]*\\[\"advertiser_id\"\\][\\s\\S]*$"));
		} catch (Error e) {
			verificationErrors.append(e.toString());
			throw e;
		}
		Thread.sleep(Long.valueOf((String) configuration
				.getProperty("thread.wait.time1")));
	}

	// driver.close();
	@Test(priority = 9)
	public void TagServing() throws InterruptedException, IOException {
		String url4 = (String) configuration.getProperty("url.name4");
		getDriver().get(getBaseUrl() + url4);
		//driver.get(baseUrl + url4);
		driver.findElement(By.id("searchtext")).sendKeys("saurabh");
		driver.findElement(By.name("action_search")).click();
		driver.findElement(By.linkText("saurabh.solanki@vdopia.com")).click();
		driver.close();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		driver.findElement(By.cssSelector("b")).click();
		driver.findElement(By.linkText("View App")).click();
		new Select(
				driver.findElement(By
						.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[9]/select")))
				.selectByVisibleText("Full-page VDO");
		Thread.sleep(Long.valueOf((String) configuration
				.getProperty("thread.wait.time1")));
		driver.findElement(
				By.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[9]/input[4]"))
				.click();
		String var = driver
				.findElement(
						By.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[9]/span/textarea"))
				.getText();
		System.out.println(var);
		String var1 = var.substring(
				var.indexOf("<script language='javascript'") + 35,
				var.lastIndexOf("</script>") - 2);
		System.out.println(var1);
		driver.get(var1);
		try {
			assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
					.matches("^[\\s\\S]*\"inventory\":\"available\"[\\s\\S]*$"));
		} catch (Error e) {
			verificationErrors.append(e.toString());
			throw e;
		}
		String contain = driver.findElement(By.cssSelector("BODY")).getText();
	    String split = contain.substring(
				contain.indexOf("iVDOAds({") + 35,
				contain.lastIndexOf("'iVDOAds');") - 35);
	    String Tag1 = "vdo_tvi_0";
	    String Tag2 = "vdo_tvi_25";
	    String Tag3 = "vdo_tvi_50";
	    String Tag4 = "vdo_tvi_75";
	    String Tag5 = "vdo_tmu";
	    String Tag6 = "vdo_tvi";
	    String Tag7 = "vdo_tae";
	    String Tag8 = "vdo_tcl";
	    String Tag9 = "vdo_tum";
		System.out.print(split);
		if(split.contains(Tag1))
		{
		try {
			assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
					.matches("^[\\s\\S]*\"vdo_tvi_0\"[\\s\\S]*$"));
			//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
		} catch (Error e) {
			verificationErrors.append(e.toString());
			throw e;
		}	
		}
		else
		{
			System.out.print("Tag vdo_tvi_0 is not present");
		}
	if(split.contains(Tag2))
	{
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"vdo_tvi_25\"[\\s\\S]*$"));
		//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}	
	}
	else
	{
		System.out.print("Tag vdo_tvi_25 not found");
	}
	if(split.contains(Tag3))
	{
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"vdo_tvi_50\"[\\s\\S]*$"));
		//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}	
	}
	else
	{
		System.out.print("Tag vdo_tvi_50 not found");
	}
	if(split.contains(Tag4))
	{
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"vdo_tvi_75\"[\\s\\S]*$"));
		//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}	
	}
	else
	{
		System.out.print("Tag vdo_tvi_75 not found");
	}
	if(split.contains(Tag5))
	{
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"vdo_tmu\"[\\s\\S]*$"));
		//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}	
	}
	else
	{
		System.out.print("Tag vdo_tmu not found");
	}
	
	if(split.contains(Tag6))
	{
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"vdo_tvi\"[\\s\\S]*$"));
		//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}	
	}
	else
	{
		System.out.print("Tag vdo_tvi not found");
	}
	if(split.contains(Tag7))
	{
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"vdo_tae\"[\\s\\S]*$"));
		//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}	
	}
	else
	{
		System.out.print("Tag vdo_tae not found");
	}
	
	if(split.contains(Tag8))
	{
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"vdo_tcl\"[\\s\\S]*$"));
		//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}	
	}
	else
	{
		System.out.print("Tag vdo_tcl not found");
	}

	if(split.contains(Tag9))
	{
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"vdo_tum\"[\\s\\S]*$"));
		//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}	
	}
	else
	{
		System.out.print("Tags vdo_tum does not found");
	}
	
		Thread.sleep(Long.valueOf((String) configuration
				.getProperty("thread.wait.time1")));
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
	}

	@Test(priority = 10)
	public void Logout() {
		driver.get(baseUrl + "/vadmins/?page=login");
		driver.findElement(By.linkText("Logout")).click();
	}

	public String captureScreen() {

	    String path;
	    try {
	        WebDriver augmentedDriver = new Augmenter().augment(driver);
	        File source = ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.FILE);
	        path = "./target/screenshots/" + source.getName();
	        FileUtils.copyFile(source, new File(path)); 
	    }
	    catch(IOException e) {
	        path = "Failed to capture screenshot: " + e.getMessage();
	    }
	    return path;

	}
	@AfterTest
	public void tearDown() throws Exception {
		driver.quit();
		//SendResults sr = new SendResults("saurabh.solanki@vdopia.com",
				//"tech-ops-issues@vdopia.com", "Title email", "Message", null);
		//sr.sendTestNGResult();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			AssertJUnit.fail(verificationErrorString);
		}
	}

	
}
