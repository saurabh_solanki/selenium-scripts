package CPOT;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PublisherwithoutOverride {
  private WebDriver driver;
  private String baseUrl;
  private StringBuffer verificationErrors = new StringBuffer();
private String publisher1;
private String publisher2;
private String publisher3;
private String publisher4;
private String publisher5;
private String publisher6;
private String publisher7;
private String publisher8;
private String publisher9;
private String publisher10;
private String pub_revenue1;
private String test;
private String pubrevenue;
private String match;
private float number1;
private float number2;
private Integer intValue;
private Integer intValue2;
  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://vdopia.com";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testWithoutoverride() throws Exception {
    driver.get(baseUrl + "/vadmins/?page=login");
    driver.findElement(By.id("googleLogin")).click();
    driver.findElement(By.id("Email")).clear();
    driver.findElement(By.id("Email")).sendKeys("saurabh.solanki@vdopia.com");
    driver.findElement(By.id("Passwd")).clear();
    driver.findElement(By.id("Passwd")).sendKeys("sau123456");
    driver.findElement(By.id("signIn")).click();
    driver.findElement(By.linkText("Tools")).click();
    driver.findElement(By.id("googleLogin")).click();
    driver.findElement(By.linkText("Tools")).click();
    driver.findElement(By.linkText("Query Browser Report")).click();
    driver.findElement(By.name("query")).clear();
    driver.findElement(By.name("query")).sendKeys("select sum(pub_revenue_without_channel_price_override),pub_email from pub_earnings_2013_09 where device='iphone' and channel_type='iphone' group by pub_id order by sum(pub_revenue) desc limit 10;");
    driver.findElement(By.name("runsubmit")).click();
    WebElement table = driver.findElement(By.id("fi-approvaltable")); 
    List<WebElement> allRows = table.findElements(By.tagName("tr")); 
 // And iterate over them, getting the cells 
   for (WebElement row : allRows) { 
     List<WebElement> cells = row.findElements(By.xpath("./*")); 

 for (WebElement cell : cells) { 
     System.out.println(cell.getText());
 }
    //Thread.sleep(50000);
    publisher1 = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr[1]/td[2])[2]")).getText();
    pubrevenue = driver.findElement(By.cssSelector("#DivMainContent > #TblId > #fi-approvaltable > tbody > tr > td")).getText();
    System.out.print(publisher1);
    System.out.print(pubrevenue);
    Float number1 = Float.parseFloat(pubrevenue);
    intValue = Integer.valueOf(number1.intValue());
    //System.out.print(number1);
    System.out.print(intValue);
    driver.findElement(By.linkText("Logout")).click();
    driver.get(baseUrl + "/vadmins/?page=login");
    /*driver.findElement(By.id("googleLogin")).click();
    driver.findElement(By.id("Email")).clear();
    driver.findElement(By.id("Email")).sendKeys("saurabh.solanki@vdopia.com");
    driver.findElement(By.id("Passwd")).clear();
    driver.findElement(By.id("Passwd")).sendKeys("sau123456");
    driver.findElement(By.id("signIn")).click();*/
    driver.findElement(By.linkText("Tools")).click();
    driver.findElement(By.id("googleLogin")).click();
    driver.findElement(By.linkText("Tools")).click();
    driver.findElement(By.linkText("Query Browser Report")).click();
    driver.findElement(By.name("query")).clear();
    driver.findElement(By.name("query")).clear();
    driver.findElement(By.name("query")).sendKeys("select channels.publisher_id,sum(pubrevenue_advcurrency*ex_rate_advpub) from daily_stats ds left join channels on channels.id=ds.channel_id where channels.publisher_id=2846 and ds.date>='2013-09-01' and ds.date<='2013-09-30';");
    driver.findElement(By.name("runsubmit")).click();
    match = driver.findElement(By.xpath("(//table[@id='fi-approvaltable']/tbody/tr/td[2])[2]")).getText();
   // System.out.print(match);
    Float number2 = Float.parseFloat(match);
    intValue2 = Integer.valueOf(number2.intValue());
    System.out.print(intValue2);
    Assert.assertEquals(intValue,intValue2);
    System.out.print("Reports are matched");
    driver.findElement(By.linkText("Logout")).click();
  }
}
}