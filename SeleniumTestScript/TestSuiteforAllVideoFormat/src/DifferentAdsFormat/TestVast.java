package DifferentAdsFormat;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

import org.testng.Assert;
import org.testng.AssertJUnit;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.After;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

@SuppressWarnings("unused")
public class TestVast {
	static PropertiesConfiguration configuration;
	static {
		final String PROPERTIES_FILE_NAME = "C:\\Users\\Saurabh\\workspace\\TestSuiteforAllVideoFormat\\conf\\configuration.properties";
		configuration = new PropertiesConfiguration();
		try {
			configuration.load(PROPERTIES_FILE_NAME);
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}

	}
	
	public static PropertiesConfiguration getConfiguration() {
		return configuration;
	}

	
	public static void setConfiguration(PropertiesConfiguration configuration) {
		TestVast.configuration = configuration;
	}

	
	public WebDriver getDriver() {
		return driver;
	}

	
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	
	public String getBaseUrl() {
		return baseUrl;
	}

	
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	
	public StringBuffer getVerificationErrors() {
		return verificationErrors;
	}

	
	public void setVerificationErrors(StringBuffer verificationErrors) {
		this.verificationErrors = verificationErrors;
	}

	
	public static String getResult() {
		return result;
	}

	
	public static void setResult(String result) {
		TestVast.result = result;
	}

	
	public String getCampaign() {
		return campaign;
	}

	
	public void setCampaign(String campaign) {
		this.campaign = campaign;
	}

	
	public String getSearch() {
		return search;
	}

	
	public void setSearch(String search) {
		this.search = search;
	}

	/**
	 * @return the finalDate
	 */
	public String getFinalDate() {
		return finalDate;
	}

	
	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}
  private WebDriver driver;
  private String baseUrl;
  private StringBuffer verificationErrors = new StringBuffer();
  private static String result;
  private String campaign;
  public String search;
  public String finalDate;
  public String Publisher;

@BeforeTest
  public void setUp() throws Exception {

	System.setProperty("webdriver.firefox.bin",
			(String) configuration.getProperty("webdriver.firefox.bin"));
    driver = new FirefoxDriver();
    baseUrl = "http://mobile.qa2.vdopia.com";
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test(priority = 0)
  public void GmailLogin() throws Exception {
	    
	  //  DateFormat dateFormat2 = new SimpleDateFormat("dd"); 
	  //  Date date2 = new Date();
	    
	    driver.get(baseUrl + "/vadmins/?page=login");
		driver.findElement(By.id("googleLogin")).click();
	    driver.findElement(By.id("Email")).clear();
	    String user = (String) configuration.getProperty("gmail.name");
		String Pass = (String) configuration.getProperty("gmail.password");
	    driver.findElement(By.id("Email")).sendKeys(user);
	    driver.findElement(By.id("Passwd")).clear();
	    driver.findElement(By.id("Passwd")).sendKeys(Pass);
	    driver.findElement(By.id("signIn")).click();
  }  
  @Test(priority = 1)
	public void PublisherSearch() {
	  
	    driver.findElement(By.linkText("Publishers")).click();
	    driver.findElement(By.id("searchtext")).sendKeys("saurabh");
	    driver.findElement(By.name("action_search")).click();
	    driver.findElement(By.linkText("saurabh.solanki@vdopia.com")).click();
	    Publisher = driver.findElement(By.linkText("saurabh.solanki@vdopia.com")).getText();
	    System.out.print(Publisher);
	    driver.close();    
       }
  
  @Test(priority = 2)
	public void ChannelCreation() {
	  
           DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy:HH:mm"); 
           Date date = new Date();
           finalDate = dateFormat.format(date);
  
	    for(String winHandle : driver.getWindowHandles()) {
	        driver.switchTo().window(winHandle);
	      }
	    driver.findElement(By.cssSelector("b")).click();
	    //System.out.print(FinalDate);
	    driver.findElement(By.id("name")).clear();
	    driver.findElement(By.id("name")).sendKeys("App on" + (finalDate));
	    // ERROR: Caught exception [Error: Dom locators are not implemented yet!]
	    driver.findElement(By.id("app_type_web")).click();
	    driver.findElement(By.id("url")).clear();
	    driver.findElement(By.id("url")).sendKeys("www.testingworld.com");
	    driver.findElement(By.id("submit")).click();
	    driver.findElement(By.xpath("(//a[contains(text(),'Test Mode')])[last()]")).click();
	    driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[last()]/td[2]/div/input")).click();
	    search = driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[4]")).getText();
  }
  
  @Test(priority = 3)
	public void AdvertiserLogin() throws IOException {
	  String url2 = (String) configuration.getProperty("url.name2");
	  getDriver().get(getBaseUrl() + url2);
      driver.findElement(By.linkText("Advertisers")).click();
       //driver.findElement(By.id("filtertext")).clear();
       driver.findElement(By.id("searchtext")).sendKeys("suprit");
	   driver.findElement(By.name("action_search")).click();
	   driver.findElement(By.linkText("suprit@vdopia.com")).click();
	   driver.close();
    for(String winHandle : driver.getWindowHandles()){
        driver.switchTo().window(winHandle);
      }
    //System.out.println("Title of the page after - switchingTo: " + driver.getTitle());
  }
  @Test(priority = 4)
	public void CreateVast() throws InterruptedException {  
        driver.findElement(By.xpath("//input[@value='Create New Ad Campaign']")).click();
	    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	    //Thread.sleep(2000);
	    driver.findElement(By.id("vastfeedadvertisement")).click();
	    driver.findElement(By.id("minivdo")).click();
	    driver.findElement(By.id("vastfeedtype")).click();
	    driver.findElement(By.cssSelector("input.submit")).click();
	    driver.findElement(By.id("vastfeedurl")).clear();
	    driver.findElement(By.id("vastfeedurl")).sendKeys("http://i2.vdopia.com/dev/pankaj_adserver/krishna/avi.xml");
	    driver.findElement(By.id("vdovalidate")).click();
	    Thread.sleep(5000);
	    Alert javascriptAlert = driver.switchTo().alert();
	    System.out.println(javascriptAlert.getText()); // Get text on alert box
	    javascriptAlert.accept();
	    driver.findElement(By.id("vastfeedname")).clear();
	    driver.findElement(By.id("vastfeedname")).sendKeys("Vast Saurabh");
	    driver.findElement(By.id("saveButtom")).click();
	    driver.findElement(By.xpath("//input[@value='Save & Continue >>']")).click();
  }
	    @Test(priority = 5)
		public void Targeting() throws InterruptedException {
	    driver.findElement(By.id("choosegeotargeting")).click();
	    driver.findElement(By.id("cntIN")).click();
	    driver.findElement(By.id("choosesitetargeting")).click();
	    driver.findElement(By.id("sss")).sendKeys((search));
	    Thread.sleep(20000);
	    driver.findElement(By.id("sss")).sendKeys(Keys.RETURN);
	    driver.findElement(By.id("site1")).sendKeys((search));
	    String Channel1 = "span[title=\"" + search + " (Vdopia->" + Publisher + ")\"]";
	    System.out.print(Channel1);
	    driver.findElement(By.cssSelector(Channel1)).click();
	    Thread.sleep(20000);
	    driver.findElement(By.cssSelector("input.leftarrow")).click();
	    Thread.sleep(20000);
	    driver.findElement(By.id("submit")).click();
	    }
	    
@Test(priority = 6)
		public void Budget() {
	    new Select(driver.findElement(By.id("model"))).selectByVisibleText("CPCV");
	    driver.findElement(By.id("maxcpm")).clear();
	    driver.findElement(By.id("maxcpm")).sendKeys("1");
	    driver.findElement(By.id("dailylimit")).clear();
	    driver.findElement(By.id("dailylimit")).sendKeys("10");
	    driver.findElement(By.id("budget")).clear();
	    driver.findElement(By.id("budget")).sendKeys("1000");
	    new Select(driver.findElement(By.name("priority_level"))).selectByVisibleText("Best Effort");
	    new Select(driver.findElement(By.name("pacing_type"))).selectByVisibleText("Uniform");
	    new Select(driver.findElement(By.name("timezone"))).selectByVisibleText("IST (Indian Standard Time)");
	    
	    // Selecting Current Date
	    driver.findElement(By.id("start_date")).click();
	    DateFormat dateFormat2 = new SimpleDateFormat("dd"); 
	    Date date2 = new Date();
		String today = dateFormat2.format(date2);
		System.out.print(today);
	    //driver.findElement(By.xpath("//div[@id='dp-calendar']/table/tbody/tr[3]/td[5]")).click();
	    WebElement dateWidget = driver.findElement(By.id("dp-calendar"));  
	    List<WebElement> columns=dateWidget.findElements(By.tagName("td"));  

		for (WebElement cell : columns) {
		if (cell.getText().equals(today)) {
		cell.click();
		break;
		}

		}

	    driver.findElement(By.id("end_date")).click();
	    driver.findElement(By.xpath("//div[@id='dp-calendar']/table/tbody/tr[5]/td[6]")).click();
	    /*driver.findElement(By.id("frequencyperday")).clear();
	    driver.findElement(By.id("frequencyperday")).sendKeys("1");
	    driver.findElement(By.id("frequencyperday")).clear();
	    driver.findElement(By.id("frequencyperday")).sendKeys("10");
	    driver.findElement(By.id("frequencyperweek")).clear();
	    driver.findElement(By.id("frequencyperweek")).sendKeys("50");
	    driver.findElement(By.id("frequencypermonth")).clear();
	    driver.findElement(By.id("frequencypermonth")).sendKeys("250");*/
	    driver.findElement(By.id("action_submit")).click();
}
@Test(priority = 7)
	public void CampaignCreation() {
	    driver.findElement(By.id("campaignname")).clear();
	    driver.findElement(By.id("campaignname")).sendKeys("Vast on " +(finalDate));
	    campaign = driver.findElement(By.id("campaignname")).getAttribute("value");
	    System.out.println(campaign);
	    driver.findElement(By.id("submit")).click();
	    Alert javascriptAlert = driver.switchTo().alert();
	    System.out.println(javascriptAlert.getText()); // Get text on alert box
	    javascriptAlert.accept();
	    driver.findElement(By.name("action_approve")).click();
	    driver.get("https://mobile.qa2.vdopia.com/adv.php?page=campaign&sub=viewcamp");
	    driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[last()]/td/a")).click();
	    driver.findElement(By.linkText("Test Page")).click();
}

@Test(priority = 8)
	public void Memcamp() throws InterruptedException, IOException {

	
		driver.get("https://sb.vdopia.com/vadmins/index.php?page=login");
    driver.findElement(By.id("googleLogin")).click();
    //driver.findElement(By.id("googleLogin")).click();
    String query = "select * from memcamp where cid in (select id from campaign where name = ";
    String second = query + "'" + campaign + "');";
    System.out.print(second);
    driver.findElement(By.linkText("Tools")).click();
    
    // Please Remove Below Comments When Used in Live Portal
    //driver.findElement(By.id("googleLogin")).click();
   // driver.findElement(By.linkText("Tools")).click();
    driver.findElement(By.linkText("Query Browser Live")).click();
    driver.findElement(By.name("query")).clear();
    driver.findElement(By.name("query")).sendKeys(second);
    Thread.sleep(240000);
    driver.findElement(By.name("runsubmit")).click();
    String result = driver.findElement(By.xpath("/html/body/div/div[3]/div/div/div/div/div/div[2]/div[2]/div/table/tbody/tr/td")).getText();
    System.out.print(result);
    try {
        assertEquals("996", driver.findElement(By.cssSelector("#DivMainContent > #TblId > #fi-approvaltable > tbody > tr > td")).getText());
    } catch (Error e) {
        verificationErrors.append(e.toString());
      }
    Thread.sleep(5000);
}

@Test(priority = 9)
public void TagServing() throws InterruptedException, IOException {
	String url4 = (String) configuration.getProperty("url.name4");
	getDriver().get(getBaseUrl() + url4);
	Thread.sleep(240000);
        //driver.get("https://mobile.sb.vdopia.com/vadmins/?page=prescreen");
        driver.findElement(By.linkText("Publishers")).click();
	    driver.findElement(By.id("searchtext")).sendKeys("saurabh");
	    driver.findElement(By.name("action_search")).click();
	    driver.findElement(By.linkText("saurabh.solanki@vdopia.com")).click();
	    driver.close();
	    for(String winHandle : driver.getWindowHandles()){
	        driver.switchTo().window(winHandle);
	      }
	    driver.findElement(By.cssSelector("b")).click();
	    driver.findElement(By.linkText("View App")).click();
	    
	    //System.out.print(select);
	  // abc = driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[9]/select")).getText();
	   new Select(driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[9]/select"))).selectByVisibleText("VAST");
	   Thread.sleep(50000);
	   driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[9]/input[4]")).click();
	   String var = driver.findElement(By.xpath("/html/body/div[1]/table/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[last()]/td[9]/span/textarea")).getText();
	   System.out.println(var);
	   String var1 = var.substring(var.indexOf("https:"), var.lastIndexOf("];")+1);
	   System.out.println(var1);
	   driver.get(var1);
	   try {
			assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
					.matches("^[\\s\\S]*\"inventory\":\"available\"[\\s\\S]*$"));
		} catch (Error e) {
			verificationErrors.append(e.toString());
			throw e;
		}
	   String contain = driver.findElement(By.cssSelector("BODY")).getText();
	    String split = contain.substring(
				contain.indexOf("iVDOAds({") + 35,
				contain.lastIndexOf("'iVDOAds');") - 35);
	    String Tag1 = "vdo_tvi_0";
	    String Tag2 = "vdo_tvi_25";
	    String Tag3 = "vdo_tvi_50";
	    String Tag4 = "vdo_tvi_75";
	    String Tag5 = "vdo_tmu";
	    String Tag6 = "vdo_tvi";
	    String Tag7 = "vdo_tae";
	    String Tag8 = "vdo_tcl";
	    String Tag9 = "vdo_tum";
		System.out.print(split);
		if(split.contains(Tag1))
		{
		try {
			assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
					.matches("^[\\s\\S]*\"vdo_tvi_0\"[\\s\\S]*$"));
			//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
		} catch (Error e) {
			verificationErrors.append(e.toString());
			throw e;
		}	
		}
		else
		{
			System.out.print("Tag vdo_tvi_0 is not present");
		}
	if(split.contains(Tag2))
	{
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"vdo_tvi_25\"[\\s\\S]*$"));
		//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}	
	}
	else
	{
		System.out.print("Tag vdo_tvi_25 not found");
	}
	if(split.contains(Tag3))
	{
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"vdo_tvi_50\"[\\s\\S]*$"));
		//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}	
	}
	else
	{
		System.out.print("Tag vdo_tvi_50 not found");
	}
	if(split.contains(Tag4))
	{
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"vdo_tvi_75\"[\\s\\S]*$"));
		//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}	
	}
	else
	{
		System.out.print("Tag vdo_tvi_75 not found");
	}
	if(split.contains(Tag5))
	{
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"vdo_tmu\"[\\s\\S]*$"));
		//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}	
	}
	else
	{
		System.out.print("Tag vdo_tmu not found");
	}
	
	if(split.contains(Tag6))
	{
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"vdo_tvi\"[\\s\\S]*$"));
		//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}	
	}
	else
	{
		System.out.print("Tag vdo_tvi not found");
	}
	if(split.contains(Tag7))
	{
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"vdo_tae\"[\\s\\S]*$"));
		//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}	
	}
	else
	{
		System.out.print("Tag vdo_tae not found");
	}
	
	if(split.contains(Tag8))
	{
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"vdo_tcl\"[\\s\\S]*$"));
		//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}	
	}
	else
	{
		System.out.print("Tag vdo_tcl not found");
	}

	if(split.contains(Tag9))
	{
	try {
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*\"vdo_tum\"[\\s\\S]*$"));
		//assertEquals("zz", driver.findElement(By.cssSelector("BODY")).getText());
	} catch (Error e) {
		verificationErrors.append(e.toString());
		throw e;
	}	
	}
	else
	{
		System.out.print("Tags vdo_tum does not found");
	}
	}
	 	@Test(priority = 10)
		public void Logout() {
			driver.get(baseUrl + "/vadmins/?page=login");
			driver.findElement(By.linkText("Logout")).click();
		}
	@After
	  public void tearDown() throws Exception {
	    driver.quit();
	    String verificationErrorString = verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	    	Assert.fail(verificationErrorString);
	    }
	  }
	}

